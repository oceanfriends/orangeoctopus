﻿using OrangeOctopus.Operations;
using OrangeOctopus.Operations.Batch;
using OrangeOctopus.Operations.Contracts;
using OrangeOctopus.Operations.Process;
using OrangeOctopus.Shared;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OrangeOctopus.Operations.ConsoleRunner
{
    class Program
    {
        static void Main(string[] args)
        {
            // Get batch process
            var batchProcess = GetBatchProcess();

            // Create executor
            var executor = new Executor(new OperationFactory());

            // Execute process
            while (true)
            {
                executor.Execute(batchProcess).GetAwaiter().GetResult();

                Console.WriteLine();
                Console.WriteLine("Press any key to continue...");
                Console.ReadLine();
            }
        }

        private static AuthProperties GetAuthProps()
        {
            // https://apps.dev.microsoft.com/#/appList
            // https://developer.microsoft.com/en-us/graph/docs/concepts/auth_v2_service
            return new AuthProperties
            {
                Authority = "https://login.microsoftonline.com/" + "750cfc55-ab3a-4785-986a-aca285210979", // base url + tenant Id
                ApplicationId = "315039c5-45e3-4f9f-a197-a8bf2d80c04a", // calculation-service
                ApplicationSecret = "xcR8num62NbBqjqshjNdVer",
            };
        }

        private static ExcelOperationContract GetExcelOperationDef()
        {
            var mappings = new Dictionary<string, ExcelCell>()
            {
                ["ExcelInput1"] = new ExcelCell("Calculation", "A", 1),
                ["ExcelInput2"] = new ExcelCell("Calculation", "A", 2),
                ["ExcelOutputMultiplication"] = new ExcelCell("Calculation", "A", 3),
                ["ExcelOutputSum"] = new ExcelCell("Calculation", "A", 4),
            };
            var excelMeta = new ExcelOperationMeta(mappings, authProps: GetAuthProps(),
                    driveId: "b!UDtp5_XzbUGVF6Y4Pun6OK3B4nati_lAlio4QYgNvAexUC1OoHD1T5xQ3Y9YnwJL",
                    workbookId: "01DXZCDMVEWPIDEDQOI5HYNQXKLZUHZ7IN")
            {
            };

            return new ExcelOperationContract
            (name: "Test Operation 1",  meta: excelMeta,
                inputDefinitions: new TransputDefinition[] {
                    new TransputDefinition
                    {
                        Name = "ExcelInput1",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    },
                    new TransputDefinition
                    {
                        Name = "ExcelInput2",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    }
                },
                outputDefinitions: new TransputDefinition[] {
                    new TransputDefinition
                    {
                        Name = "ExcelOutputMultiplication",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    },
                    new TransputDefinition
                    {
                        Name = "ExcelOutputSum",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    }
                });
        }

        private static BatchProcess GetBatchProcess()
        {
            return new BatchProcess
            {
                Name = "Test Batch Process",
                Steps = new List<BatchStep>
                {
                    // Console input
                    new BatchStep
                    {
                        OperationDef = new OutputOnlyOperationContract
                        (
                            name: "Get Console Input",
                            opType: OperationType.ConsoleInput,
                            outputDefinitions: new TransputDefinition[]
                            {
                                new TransputDefinition
                                {
                                    Name = "Number1",
                                    StructureType = StructureType.Single,
                                    TypeCode = TypeCode.Double,
                                    Required = true,
                                },
                                new TransputDefinition
                                {
                                    Name = "Number2",
                                    StructureType = StructureType.Single,
                                    TypeCode = TypeCode.Double,
                                    Required = true,
                                }
                            },
                            meta: new OperationMeta()
                        )
                    },
                    // Excel calculation
                    new BatchStep
                    {
                        OperationDef = GetExcelOperationDef(),
                        PropertyMaps = new List<Map>
                        {
                            new Map
                            {
                                From = "Number1",
                                To = "ExcelInput1"
                            },
                            new Map
                            {
                                From = "Number2",
                                To = "ExcelInput2"
                            }
                        }
                    },
                    // Console output
                    new BatchStep
                    {
                        OperationDef = new InputOnlyOperationContract
                        (
                            name: "Output results to console",
                            opType: OperationType.ConsoleOutput,
                            inputDefinitions: new TransputDefinition[]
                            {
                                new TransputDefinition
                                {
                                    Name = "MultiplicationResult",
                                    StructureType = StructureType.Single,
                                    TypeCode = TypeCode.Double,
                                    Required = true,
                                },
                                new TransputDefinition
                                {
                                    Name = "SumResult",
                                    StructureType = StructureType.Single,
                                    TypeCode = TypeCode.Double,
                                    Required = true,
                                }
                            },
                            meta: new OperationMeta()
                        ),
                        PropertyMaps = new List<Map>
                        {
                            new Map
                            {
                                From = "ExcelOutputMultiplication",
                                To = "MultiplicationResult"
                            },
                            new Map
                            {
                                From = "ExcelOutputSum",
                                To = "SumResult"
                            }
                        }
                    },
                }
            };
        }

        private static List<Transput> GetInputs()
        {
            var input1Def = new TransputDefinition
            {
                Name = "Input1",
                StructureType = StructureType.Single,
                TypeCode = TypeCode.Double,
                Required = true,
            };

            var input1 = new Transput(10, input1Def);

            var input2Def = new TransputDefinition
            {
                Name = "Input2",
                StructureType = StructureType.Single,
                TypeCode = TypeCode.Double,
                Required = true,
            };

            var input2 = new Transput(5, input2Def);

            return new List<Transput> { input1, input2 };
        }
    }
}
