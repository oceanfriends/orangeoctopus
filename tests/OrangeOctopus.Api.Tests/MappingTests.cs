using System;
using Xunit;
using AutoMapper;
using OrangeOctopus.Api.Mapping;
using OrangeOctopus.Api.Mappings;
using OrangeOctopus.Operations;
using OrangeOctopus.Api.Contracts;
using System.Linq;
using OrangeOctopus.Operations.Contracts;

namespace OrangeOctopus.Api.Tests
{
    public class MapppingTests
    {
        private IMapper _automapper;
        public MapppingTests()
        {
            var config = new MapperConfiguration(cfg => cfg.AddProfile<OperationContractProfile>());
            _automapper = config.CreateMapper();
        }
        [Fact]
        public void OperationContractProfile_ShouldAssertValidMapping()
        {
            var config = new MapperConfiguration( cfg => {
                    cfg.AddProfile<OperationContractProfile>();
            });
            config.AssertConfigurationIsValid();
        }

        #region IOrangeMapper

        [Theory]
        [ClassData(typeof(ValidContractDto))]
        public void OperationDto_ShouldCreateOperationContracts(OperationDto dto)
        {
            var mappa = new OrangeMapper(_automapper);
            OperationContract mapped = mappa.Map<OperationDto, OperationContract>(dto);
            (bool samesies, string error) = ContractAndDtoEquivalent(mapped, dto);
            Assert.True(samesies, error);

        }

        [Theory]
        [ClassData(typeof(ValidExcelDto))]
        public void ExcelDto_ShouldCreateExcelContracts(OperationDto dto)
        {
            var mappa = new OrangeMapper(_automapper);
            mappa.AddEnricher<OperationDto, OperationContract>(Enrichers.EnrichOperationFromDto);
            OperationContract mapped = mappa.Map<OperationDto, OperationContract>(dto);
            (bool samesies, string error) = ContractAndDtoEquivalent(mapped, dto);
            Assert.True(samesies, error);

        }

        #endregion

        #region Enrichers
        [Fact]
        public void ExcelOperation_ShouldEnrichType()
        {
            var dto = ValidContractDto.ExcelOp;
            var mapped = _automapper.Map<OperationContract>(dto);
            var enriched = Enrichers.EnrichOperationFromDto(dto, mapped);
            Assert.True(enriched is ExcelOperationContract);
            Assert.True(enriched.Meta is ExcelOperationMeta);
        }

        [Fact]
        public void NormalOperation_ShouldNotEnrich()
        {
            var dto = ValidContractDto.BasicGet;
            var mapped = _automapper.Map<OperationContract>(dto);
            var enriched = Enrichers.EnrichOperationFromDto(dto, mapped);
            Assert.True(enriched == mapped);
            Assert.IsType<OperationContract>(enriched);
        }
        #endregion


        private (bool success, string why) ContractAndDtoEquivalent(OperationContract ctr, OperationDto dto)
        {
            bool winning = true;
            winning = ctr.Name == dto.Name;
            if (!winning) return (winning, "Name of contract and dto do not match");
            winning = ctr.Id == dto.Id;
            if (!winning) return (winning, "Id of contract and dto do not match");
            winning = ctr.InputDefinitions.Length == dto.InputDefinitions.Count();
            if (!winning) return (winning, "Number of input defs of contract and dto do not match");
            winning = ctr.OutputDefinitions.Length == dto.OutputDefinitions.Count();
            if (!winning) return (winning, "Number of output defs of contract and dto do not match");
            winning = ctr.OperationType == dto.OperationType;
            if (!winning) return (winning, "Operatoin type of contract and dto do not match");
            winning = ctr.Meta.Description == dto.Description;
            if (!winning) return (winning, "Description of contract and dto do not match");
            winning = ctr.Meta.OperationId == dto.Id;
            if (!winning) return (winning, "Operation meta of contract had an incorrect operation id");

            var inputDtoList = dto.InputDefinitions.ToList();
            foreach (var transputDef in ctr.InputDefinitions)
            {
                var found = inputDtoList.SingleOrDefault(x => x.Name == transputDef.Name);
                if (found == null) return (false, $"Could not find a input dto for the transput def name {transputDef.Name}");
                (bool won, string why) = TransputDefsEquivalent(transputDef, found);
                if (!won) return (won, why);
            }

            return (winning, "");
        }

        private (bool success, string why) TransputDefsEquivalent(TransputDefinition def, InputOutputDto dto)
        {
            bool winning = true;
            winning = def.Name == dto.Name;
            if (!winning) return (winning, "Name of transput def and dto do not match");
            winning = def.Required == dto.Required;
            if (!winning) return (winning, "Required flag for transput def and dto do not match");
            winning = def.Description == dto.Description;
            if (!winning) return (winning, "Description of transput def and dto do not match");
            winning = def.Name == dto.Name;
            if (!winning) return (winning, "Name of transput def and dto do not match");
            return (winning, "");
        }


    }
}
