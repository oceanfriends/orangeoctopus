﻿using AutoMapper;
using Benj.Common;
using Benj.Common.Data;
using Benj.Common.Entities;
using Microsoft.Extensions.Logging;
using Moq;
using OrangeOctopus.Api.Contracts;
using OrangeOctopus.Api.Controllers;
using OrangeOctopus.Api.Mapping;
using OrangeOctopus.Api.Mappings;
using OrangeOctopus.Api.Middleware;
using OrangeOctopus.Operations;
using OrangeOctopus.Operations.Persistance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace OrangeOctopus.Api.Tests
{
    public class OperationsControllerTests
    {
        private IAmARepo _repo;
        private Mock<IAmARepo> _repoMock;
        private IOrangeMapper _mappa;
        private ILogger _logger;
        private ICorrelationProvider _corr;
        public OperationsControllerTests()
        {
            _repoMock = new Mock<IAmARepo>();
            var automapper = new MapperConfiguration(cfg => cfg.AddProfile<OperationContractProfile>()).CreateMapper();
            _mappa = new OrangeMapper(automapper);
            _logger = new Mock<ILogger>().Object;
            _corr = new Mock<ICorrelationProvider>().Object;
        }
        [Fact]
        public void OperationsController_ShouldGetById()
        {
            var meta = new OperationMeta()
            {
                Endpoint = "https://jsonplaceholder.typicode.com/posts"
            };
            var fakeContract = new OperationContract("SimpleHttp", TransputDefinition.EmptyArray,
                TransputFactory.DefinitionFromType<string>("Output").ToArray(), OperationType.HttpGet, new OperationMeta());
            var registryMock = new Mock<IOperationRegistry>();
            registryMock.As<ICanCrud<OperationContract>>().Setup(svc => svc.Get(It.IsAny<string>()))
                .Returns(new ActivityResult<OperationContract>(fakeContract));

            var controller = new OperationsController(registryMock.Object, _mappa, _corr, _logger);
            var res = controller.Get(Guid.NewGuid().ToString());
            Assert.True(res.IsOk(), "Expected Ok result from Operatoins controller API, got not Ok");
            Assert.True(res.Payload != null,  "Expected hydrated payload from operation controller, got null");
            var got = res.Payload;
            Assert.True(got.Name == fakeContract.Name, $"Expected \"SuperSimple\" from operations controller, got {got.Name}");
            Assert.True(got.InputDefinitions.Count() == 1, $"Expected 1 input def from ops controller, got {got.InputDefinitions.Count()}");
            Assert.True(got.OutputDefinitions.Count() == 1, $"Expected 1 output def from ops controller, got {got.OutputDefinitions.Count()}");
            Assert.True(got.OperationType == OperationType.HttpGet, $"Expected HttpGet, got {got.OperationType}");
        }

        [Theory]
        [ClassData(typeof(ValidContractDto))]
        public void OperationsController_ShouldPostNew(OperationDto dto)
        {
            var registryMock = new Mock<IOperationRegistry>();
            registryMock.Setup(svc => svc.Create(It.IsAny<OperationContract>()))
                .Returns(new ActivityResult<OperationContract>(_mappa.Map<OperationDto, OperationContract>(dto)));

            var controller = new OperationsController(registryMock.Object, _mappa, _corr, _logger);
            var res = controller.Post(dto);
            Assert.True(res.IsOk(), "Expected Ok result from Operatoins controller save, got not Ok");
            Assert.True(res.Payload != null,  "Expected hydrated payload from operation controller save, got null");
            OperationDto saved = res.Payload;
            Assert.True(saved.Name == dto.Name, $"Expected {dto.Name} from operations controller, got {saved.Name}");
            Assert.True(saved.InputDefinitions.Count() == dto.InputDefinitions.Count(), $"Expected 1 input def from ops controller, got {saved.InputDefinitions.Count()}");
            Assert.True(saved.OutputDefinitions.Count() == dto.InputDefinitions.Count(), $"Expected 1 output def from ops controller, got {saved.OutputDefinitions.Count()}");
            Assert.True(saved.OperationType == dto.OperationType, $"Expected {dto.OperationType}, got {saved.OperationType}");
        }
        
    }
}
