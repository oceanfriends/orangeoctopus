﻿using OrangeOctopus.Api.Contracts;
using OrangeOctopus.Operations.Contracts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Api.Tests
{
    public class InputOutputDtoData : IEnumerable<object[]>
    {
        public static InputOutputDto Empty = new InputOutputDto()
        {
            Description = "I am an empty transput!",
            Name = "Empty",
            Required = true,
            Structure = ArgumentStructure.Empty,
            Type = ArgumentType.Empty
        };
        public static InputOutputDto String = new InputOutputDto()
        {
            Description = "I am a string transput!",
            Name = "StringBoi",
            Required = true,
            Structure = ArgumentStructure.Single,
            Type = ArgumentType.String
        };
        public static InputOutputDto Number = new InputOutputDto()
        {
            Description = "I am a number transput!",
            Name = "SoloNumber",
            Required = true,
            Structure = ArgumentStructure.Single,
            Type = ArgumentType.Number
        };
        public static InputOutputDto NumberTwo = new InputOutputDto()
        {
            Description = "I am a number transput!",
            Name = "DoubleDown",
            Required = true,
            Structure = ArgumentStructure.Single,
            Type = ArgumentType.Number
        };
        public static InputOutputDto NumberList = new InputOutputDto()
        {
            Description = "I am a number transput!",
            Name = "GetStacks",
            Required = true,
            Structure = ArgumentStructure.List,
            Type = ArgumentType.Number
        };

        private readonly List<object[]> list = new List<object[]>()
        {
            new [] {Empty},
            new [] {String},
            new [] {Number},
            new [] {NumberList},
        };

        public IEnumerator<object[]> GetEnumerator() => list.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    public class ValidContractDto : IEnumerable<object[]>
    {
        public static OperationDto BasicGet = new OperationDto()
        {
            Description = "My Super Cool Http Get",
            EndpointTemplate = "https://jsonplaceholder.typicode.com/posts",
            InputDefinitions = new List<InputOutputDto>() { InputOutputDtoData.Empty },
            OutputDefinitions = new List<InputOutputDto>() { InputOutputDtoData.String },
            OperationType = Operations.OperationType.HttpGet,
            Name = "HttpGetOp",
            Tags = new List<string>() { "Basic", "Dunce" }
        };

        public static OperationDto SecondGet = new OperationDto()
        {
            Description = "My second Http Get",
            EndpointTemplate = "https://jsonplaceholder.typicode.com/posts/{0}",
            InputDefinitions = new List<InputOutputDto>() { InputOutputDtoData.Number },
            OutputDefinitions = new List<InputOutputDto>() { InputOutputDtoData.String },
            OperationType = Operations.OperationType.HttpGet,
            Name = "HttpGetPost",
            Tags = new List<string>() { "Basic", "Dunce" }
        };

        public static ExcelOperationDto ExcelOp = new ExcelOperationDto()
        {
            Description = "My Super Cool Excel Calc",
            EndpointTemplate = "https://jsonplaceholder.typicode.com/posts",
            InputDefinitions = new List<InputOutputDto>() { InputOutputDtoData.Number, InputOutputDtoData.NumberTwo },
            OutputDefinitions = new List<InputOutputDto>() { InputOutputDtoData.Number, InputOutputDtoData.NumberTwo },
            OperationType = Operations.OperationType.Excel,
            Name = "Excel",
            Tags = new List<string>() { "Basic", "Dunce" },
            Id = "2",
            DriveType = DriveType.Global,
            WorkbookId = "blablabla",
            CellMappings = new Dictionary<string, ExcelCell>()
            {
                [InputOutputDtoData.Number.Name] = new ExcelCell("Worksheet1", "A", 1),
                [InputOutputDtoData.NumberTwo.Name] = new ExcelCell("Worksheet1", "A", 2),
            }
        };

        private readonly List<object[]> _data = new List<object[]>()
        {
            new [] {BasicGet},
            new [] {ExcelOp},
            new [] {SecondGet}
        };

        public IEnumerator<object[]> GetEnumerator() => _data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    public class ValidExcelDto : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new[] { ValidContractDto.ExcelOp };
            yield break;
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
