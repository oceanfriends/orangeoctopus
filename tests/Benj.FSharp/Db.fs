﻿namespace SuaveRestApi.Db

open System.Collections.Generic

type Person = {
    Id : int
    Name : string
    Age : int
    Email : string
}

module Db =

    let peopleStorage = new Dictionary<int, Person>()
    //let personOne = Person {Id=1, Name = "Ben", Age = 24, Email = "breeves997@gmail.com"}
    let personOne = {Id= 1; Name= "Ben"; Age= 24; Email= "breeves997@gmail.com"}
    let personTwo = {Id= 2; Name= "Andrew"; Age= 26; Email= "atpsullivan@microsoft.com"}
    [1, personOne; 2, personTwo] |> Seq.iter peopleStorage.Add

    let getPeople () =
        peopleStorage.Values :> seq<Person>
    let getPerson id =
        if peopleStorage.ContainsKey(id) then
            Some peopleStorage.[id]
        else
            None
    let createPerson person =
        let id = peopleStorage.Values.Count + 1
        let newPerson = {person with Id = id}
        peopleStorage.Add(id, newPerson)
        newPerson

    let updatePersonById personId personToBeUpdated =
        if peopleStorage.ContainsKey(personId) then
            let updatedPerson = {personToBeUpdated with Id = personId}
            peopleStorage.[personId] <- updatedPerson
            Some updatedPerson
        else
            None

    let updatePerson personToBeUpdated =
        updatePersonById personToBeUpdated.Id personToBeUpdated

    let deletePerson personId =
        peopleStorage.Remove(personId) |> ignore

    let isPersonExists  = peopleStorage.ContainsKey

