﻿namespace SuaveRestApi

module App =   
    open System
    open System.Threading
    open Suave.Web
    open SuaveRestApi.Rest
    open SuaveRestApi.Db
    open Suave

    [<EntryPoint>]
    let main argv = 
      let personWebPart = rest "people" {
            GetAll = Db.getPeople
            GetById = Db.getPerson
            Create = Db.createPerson
            Update = Db.updatePerson
            UpdateById = Db.updatePersonById
            Delete = Db.deletePerson
            IsExists = Db.isPersonExists
      }
      let cts = new CancellationTokenSource()
      let conf = { defaultConfig with cancellationToken = cts.Token }
      let listening, server = startWebServerAsync conf (choose [personWebPart;(Successful.OK "Hello World")])
        
      Async.Start(server, cts.Token)
      printfn "Make requests now"
      Console.ReadKey true |> ignore
        
      cts.Cancel()

      0 // return an integer exit code
