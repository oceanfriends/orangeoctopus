﻿using Aq.ExpressionJsonSerializer;
using Benj.Calculation.Tests.IntegrationTests;
using Benj.Common;
using Benj.Common.Data;
using Newtonsoft.Json;
using OrangeOctopus.Operations.Persistance;
using OrangeOctopus.Operations.Process;
using OrangeOctopus.Operations.Contracts;
using OrangeOctopus.Shared;
using SuaveRestApi.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace OrangeOctopus.Operations.Tests
{
    public class OperationRegistryTests
    {
        private ICanCrud _repo;
        private IOperationFactory _factory;
        public OperationRegistryTests()
        {

            var settings = new JsonSerializerSettings();
            settings.Converters.Add(new ExpressionJsonConverter(Assembly.GetAssembly(typeof(Transput))));
            //use serializing repo because it is critical that the factory handles 
            //serialization mapping correctly, and we catch a lot of errors by forcing serializatoin
            var repo = new SimpleSerializingRepo(x => x.GetHashCode().ToString(), settings);
            repo.AddKeyStrategy<IOperationService>(x => x.Contract.Id);
            repo.AddKeyStrategy<OperationContract>(x => x.Id);
            repo.AddKeyStrategy<ExcelOperationContract>(x => x.Id);
            repo.AddKeyStrategy<ExpressionOperationContract>(x => x.Id);
            repo.AddKeyStrategy<OperationMeta>(x => x.OperationId);
            repo.AddKeyStrategy<ExpressionOperationMeta>(x => x.OperationId);
            repo.AddKeyStrategy<ExcelOperationMeta>(x => x.OperationId);
            _repo = repo;
            _factory = new OperationFactory();
        }

        [Fact]
        public async Task ShouldSaveInMemoryService()
        {
            var registry = new OperationRegistry(_repo, _factory);
            IOperationService svc = OperationFactory.Create<double, double>(x => 5 * x, "GETFIVEBYFIVE");
            var saved = registry.Create(svc);
            TestHelper.AssertOk(saved);
            OperationContract savedContrat = saved.Payload.Contract;
            var res = await saved.Payload.ExecuteAsync(TransputFactory.CreateFrom(5d, savedContrat.InputDefinitions[0]));
            Assert.True(res != null);
            Assert.True((double)res[0].Value == 25);

        }

        [Fact]
        public async Task ShouldSaveExpressionService()
        {
            Expression<Func<Transput[], Transput[]>> expression = inputs => TransputFactory.CreateFrom((double)inputs[0].Value * 5, "OUT:TEST").ToArray();
            var inputDef = new[] { TransputFactory.DefinitionFromType<double>("IN:TEST") };
            var outputDef = TransputFactory.DefinitionFromType<double>("OUT:TEST").ToArray();

            var registry = new OperationRegistry(_repo, _factory);
            IOperationService svc = new ExpressionOperation("TEST", expression, inputDef, outputDef);
            var saved = registry.Create(svc);
            TestHelper.AssertOk(saved);
            OperationContract savedContrat = saved.Payload.Contract;
            var res = await saved.Payload.ExecuteAsync(TransputFactory.CreateFrom(5d, savedContrat.InputDefinitions[0]));
            Assert.True(res != null);
            Assert.True((double)res[0].Value == 25);
        }

        [Fact]
        public void ShouldSaveExpressionContract()
        {
            Expression<Func<Transput[], Transput[]>> expression = inputs => TransputFactory.CreateFrom((double)inputs[0].Value * 5, "OUT:TEST").ToArray();
            var inputDef = new[] { TransputFactory.DefinitionFromType<double>("IN:TEST") };
            var outputDef = TransputFactory.DefinitionFromType<double>("OUT:TEST").ToArray();

            var registry = new OperationRegistry(_repo, _factory);
            var meta = new ExpressionOperationMeta(expression);
            var ctr = new ExpressionOperationContract("ExpressionTest", inputDef, outputDef, meta);
            var saved = registry.Create(ctr);
            TestHelper.AssertOk(saved);
            OperationContract savedContrat = saved.Payload;
            Assert.True(savedContrat != null);
            Assert.True(savedContrat.InputDefinitions.Length == ctr.InputDefinitions.Length);
            Assert.True(savedContrat.OutputDefinitions.Length == ctr.OutputDefinitions.Length);
            Assert.True(savedContrat.Name == ctr.Name);
            ExpressionOperationMeta exprMeta = (ExpressionOperationMeta)(savedContrat.Meta);
            Assert.True(exprMeta.ExpressionRepresentation != null);
        }

        [Fact]
        public void ShouldSaveExcelService()
        {
            var inputDef = new[] { TransputFactory.DefinitionFromType<double>("ExcelInput1"),
                TransputFactory.DefinitionFromType<double>("ExcelInput2") };
            var outputDef = TransputFactory.DefinitionFromType<double>("ExcelOutput").ToArray();

            var registry = new OperationRegistry(_repo, _factory);

            var mappings = new Dictionary<string, ExcelCell>()
            {
                ["ExcelInput1"] = new ExcelCell("Sheet1", "A", 1),
                ["ExcelInput2"] = new ExcelCell("Sheet1", "A", 2),
                ["ExcelOutput"] = new ExcelCell("Sheet1", "A", 3),
            };

            var excelMeta = new ExcelOperationMeta(mappings, AuthProperties._andrewAuthProperties, "abc", "123");
            var excelContract = new ExcelOperationContract("ExcelTest", inputDef, outputDef, excelMeta);
            IOperationService svc = new ExcelOperation(excelContract, new ExcelAuthService());
            var saved = registry.Create(svc);
            TestHelper.AssertOk(saved);
            OperationContract savedContrat = saved.Payload.Contract;
            Assert.True(savedContrat != null);
            var ectr = savedContrat as ExcelOperationContract;
            Assert.True(ectr != null);
            (bool samesies, string error) = TestHelper.ExcelContractsEquivalent(excelContract, ectr);
            Assert.True(samesies, error);
        }

        [Fact]
        public  void ShouldSaveExcelContract()
        {
            var inputDef = new[] { TransputFactory.DefinitionFromType<double>("ExcelInput1"),
                TransputFactory.DefinitionFromType<double>("ExcelInput2") };
            var outputDef = TransputFactory.DefinitionFromType<double>("ExcelOutput").ToArray();

            var registry = new OperationRegistry(_repo, _factory);

            var mappings = new Dictionary<string, ExcelCell>()
            {
                ["ExcelInput1"] = new ExcelCell("Sheet1", "A", 1),
                ["ExcelInput2"] = new ExcelCell("Sheet1", "A", 2),
                ["ExcelOutput"] = new ExcelCell("Sheet1", "A", 3),
            };
            var excelMeta = new ExcelOperationMeta(mappings, AuthProperties._andrewAuthProperties, "abc", "123")
            {
            };
            var excelContract = new ExcelOperationContract("ExcelTest", inputDef, outputDef, excelMeta);
            var saved = registry.Create(excelContract);
            TestHelper.AssertOk(saved);
            OperationContract savedContrat = saved.Payload;
            (bool samesies, string error) = TestHelper.ExcelContractsEquivalent(excelContract, (ExcelOperationContract)savedContrat);
            Assert.True(samesies, error);
        }

        [Fact]
        public async Task ShouldSaveHttpService()
        {
            var meta = new OperationMeta()
            {
                Description = "Hello world suave server",
                Endpoint = WebServerFixture.RootUrl,
                ResourceUriBuilder = null
            };

            var contract = new OperationContract("I can haz web server?", 
                new[] { TransputDefinition.Empty }, 
                TransputFactory.DefinitionFromType<string>("hi").ToArray(),
                OperationType.HttpGet,
                meta);
            var svc = new BaseHttpGetOperation(contract);

            var registry = new OperationRegistry(_repo, _factory);
            var saved = registry.Create(svc);
            TestHelper.AssertOk(saved);
            var res = await saved.Payload.ExecuteAsync();
            Assert.True(res != null);
            Assert.True((string)res[0].Value == "Hello World");
        }

        [Fact]
        public void ShouldSaveHttpContract()
        {
            var meta = new OperationMeta()
            {
                Description = "Hello world suave server",
                Endpoint = WebServerFixture.RootUrl,
                ResourceUriBuilder = null
            };

            var contract = new OperationContract("I can haz web server?", 
                new[] { TransputDefinition.Empty }, 
                TransputFactory.DefinitionFromType<string>("hi").ToArray(),
                OperationType.HttpGet,
                meta);

            var registry = new OperationRegistry(_repo, _factory);
            var saved = registry.Create(contract);
            TestHelper.AssertOk(saved);
            OperationContract ctr = saved.Payload;
            Assert.True(ctr != null);
            Assert.True(ctr.Name == contract.Name);
            Assert.True(ctr.InputDefinitions.Length == contract.InputDefinitions.Length);
            Assert.True(ctr.OutputDefinitions.Length == contract.OutputDefinitions.Length);
            Assert.True(ctr.Meta.Endpoint == contract.Meta.Endpoint);

        }

        [Fact]
        public async Task ShouldSaveTypedHttpService()
        {
            Expression<Func<Transput[], string>> uriBuilder = id => String.Format(WebServerFixture.RootUrl + "people/{0}", id[0].Value);
            var svc = new HttpGetOperation<int, Person>(uriBuilder, "I can haz web server?");

            var registry = new OperationRegistry(_repo, _factory);
            var saved = registry.Create(svc);
            TestHelper.AssertOk(saved);
            var res = await saved.Payload.ExecuteAsync(TransputFactory.CreateFrom(1, saved.Payload.Contract.InputDefinitions[0].Name));
            Assert.True(res != null);
            string raw = (string)res[0].Value;
            var person = JsonConvert.DeserializeObject<Person>(raw);
            Assert.True(person.Id == 1);
        }
        
    }
}
