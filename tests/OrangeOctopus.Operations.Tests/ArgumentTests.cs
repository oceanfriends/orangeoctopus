﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace OrangeOctopus.Operations.Tests
{
    public class ArgumentTests
    {
        #region arg creation
        [Fact]
        public void BasicTypes_ShouldCreateArgDefs()
        {
            var s = TransputFactory.DefinitionFromType<string>("s");
            Assert.True(s.TypeCode == TypeCode.String);
            Assert.True(s.Required);
            Assert.True(s.StructureType == StructureType.Single);
            Assert.True(s.Name == "s");


            var d = TransputFactory.DefinitionFromType<double>("d");
            Assert.True(d.TypeCode == TypeCode.Double);
            Assert.True(d.Required);
            Assert.True(d.StructureType == StructureType.Single);
            Assert.True(d.Name == "d");

            var b = TransputFactory.DefinitionFromType<bool>("b");
            Assert.True(b.TypeCode == TypeCode.Boolean);
            Assert.True(b.Required);
            Assert.True(b.StructureType == StructureType.Single);
            Assert.True(b.Name == "b");

            var dt = TransputFactory.DefinitionFromType<DateTime>("dt");
            Assert.True(dt.TypeCode == TypeCode.DateTime);
            Assert.True(dt.Required);
            Assert.True(dt.StructureType == StructureType.Single);
            Assert.True(dt.Name == "dt");

        }

        [Fact]
        public void BasicTypes_ShouldCreateParamDefs()
        {
            Transput s = TransputFactory.CreateFrom("s", "stringy-string-boy");
            Assert.True((string)s.Value == "s");
            Assert.True(s.Definition.Equivalent(TestHelper.StringDefinition));

            s = TransputFactory.CreateFrom(12d, "stringy-string-boy");
            Assert.True((double)s.Value == 12d);
            Assert.True(s.Definition.Equivalent(TestHelper.DoubleDefinition));
        }
        #endregion  

    }
}
