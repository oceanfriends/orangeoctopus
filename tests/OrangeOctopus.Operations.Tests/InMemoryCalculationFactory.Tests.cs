﻿using Benj.Common;
using OrangeOctopus.Operations.Process;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace OrangeOctopus.Operations.Tests
{
    public class InMemoryCalculationFactoryTests
    {
        #region Actions
        [Fact]
        public void InMemFactory_ShouldCreateAnonymousDelegate()
        {
            IOperationService svc = OperationFactory.Create(() => { }, "Do nothing!");
            Assert.NotNull(svc);
        }
        [Fact]
        public void InMemFactory_CreatingAction_ShouldValidatePass()
        {
            Action del = new Action(() => { });
            IOperationService svc = OperationFactory.Create(del, "MyDel");
            var res = svc.ValidateArguments();
            Assert.True(res.Status == StatusCode.Ok);

            res = svc.ValidateArguments(new Transput[] { });
            Assert.True(res.Status == StatusCode.Ok);

            res = svc.ValidateArguments(Transput.None);
            Assert.True(res.Status == StatusCode.Ok);
        }
        [Fact]
        public void InMemFactory_CreatingAction_ShouldValidateFail()
        {
            Action del = new Action(() => { });
            IOperationService svc = OperationFactory.Create(del, "MyDel");
            var res = svc.ValidateArguments(TestHelper.NumParameter);
            Assert.True(res.Status == StatusCode.Failed);
        }
        [Fact]
        public async Task InMemFactory_CreatingAction_ShouldExecute_NoParamsAsync()
        {
            int x = 0;
            Action del = new Action(() => x = 1);
            IOperationService svc = OperationFactory.Create(del, "MyDel");
            var res = await svc.ExecuteAsync();
            Assert.True(TestHelper.IsEmptyResponse(res[0]));
            Assert.True(x == 1);
        }
        [Fact]
        public async Task InMemFactory_CreatingAction_ShouldExecute_EmptyParamAsync()
        {
            int x = 0;
            Action del = new Action(() => x = 1);
            IOperationService svc = OperationFactory.Create(del, "MyDel");
            var res = await svc.ExecuteAsync(Transput.None);
            Assert.True(TestHelper.IsEmptyResponse(res[0]));
            Assert.True(x == 1);
        }

        [Fact]
        public void InMemFactory_CreatingAction_ShouldNotExecute_PassInput()
        {
            int x = 0;
            Action del = new Action(() => x = 1);
            IOperationService svc = OperationFactory.Create(del, "MyDel");
            var input = TestHelper.NumParameter;
            Assert.ThrowsAnyAsync<InvalidParamatersException>(async () => await svc.ExecuteAsync(input));
        }
        //[Fact]
        //public void InMemFactory_CreatingAsyncAction_ShouldExecute_NoParams()
        //{
        //    int x = 0;
        //    Action del = new Action(async () =>
        //    {
        //        await Task.Delay(100);
        //        x = 1;
        //    });
        //    ICalculationService svc = InMemoryCalculationFactory.Create(del, "MyDel");
        //    svc.Execute();
        //    Assert.True(x == 1);
        //}

        [Fact]
        public async void InMemFactory_CreatingActionWithNumInput_ShouldExecute()
        {
            List<double> check = new List<double>();
            Action<double> action = new Action<double>(x => check.Add(x));
            IOperationService svc = OperationFactory.Create<double>(action, "MultiplyByTwo");
            var input = TestHelper.NumParameter;
            input.Definition.Name = svc.Contract.InputDefinitions[0].Name;

            var res = await svc.ExecuteAsync(input);
            Assert.True(check.Count == 1 && check[0] == (double)input.Value);
        }
        [Fact]
        public void InMemFactory_CreatingActionWithNumInput_ShouldValidateSuccess()
        {
            List<double> check = new List<double>();
            Action<double> action = new Action<double>(x => check.Add(x));
            IOperationService svc = OperationFactory.Create<double>(action, "MultiplyByTwo");
            var input = TestHelper.NumParameter;
            input.Definition.Name = svc.Contract.InputDefinitions[0].Name;
            var res = svc.ValidateArguments(input);
            Assert.True(res.Status == StatusCode.Ok);
        }
        [Fact]
        public void InMemFactory_CreatingActionWithNumInput_ShouldValidateFailure()
        {
            List<double> check = new List<double>();
            Action<double> action = new Action<double>(x => check.Add(x));
            IOperationService svc = OperationFactory.Create<double>(action, "MultiplyByTwo");
            var input = Transput.None;
            var res = svc.ValidateArguments(input);
            Assert.True(res.Status == StatusCode.Failed);

            res = svc.ValidateArguments();
            Assert.True(res.Status == StatusCode.Failed);

            res = svc.ValidateArguments(new Transput[] { });
            Assert.True(res.Status == StatusCode.Failed);

            res = svc.ValidateArguments(new Transput[] { TestHelper.StringParameter });
            Assert.True(res.Status == StatusCode.Failed);
        }

        [Fact]
        public async Task InMemFactory_CreatingActionWithStringInput_ShouldExecuteAsync()
        {
            List<string> checka = new List<string>();
            Action<string> action = new Action<string>(x => checka.Add(x));
            IOperationService svc = OperationFactory.Create<string>(action, "StringyStringBoy");
            var input = TestHelper.StringParameter;
            input.Definition.Name = svc.Contract.InputDefinitions[0].Name;
            var res = await svc.ExecuteAsync(input);
            Assert.True(checka.Count == 1 && checka[0] == (string)input.Value);
        }
        [Fact]
        public void InMemFactory_CreatingActionWithStringInput_ShouldValidateSuccess()
        {
            Action<string> action = new Action<string>(x => x = "Wow ben you're cool");
            IOperationService svc = OperationFactory.Create<string>(action, "StringyStringBoy");
            var input = TestHelper.StringParameter;
            input.Definition.Name = svc.Contract.InputDefinitions[0].Name;
            var res = svc.ValidateArguments(input);
            Assert.True(res.Status == StatusCode.Ok);
        }
        [Fact]
        public void InMemFactory_CreatingActionWithStringInput_ShouldValidateFailure()
        {
            Action<string> action = new Action<string>(x => x = "Wow ben you're cool");
            IOperationService svc = OperationFactory.Create<string>(action, "StringyStringBoy");
            var input = Transput.None;
            var res = svc.ValidateArguments(input);
            Assert.True(res.Status == StatusCode.Failed);

            res = svc.ValidateArguments();
            Assert.True(res.Status == StatusCode.Failed);

            res = svc.ValidateArguments(new Transput[] { });
            Assert.True(res.Status == StatusCode.Failed);

            res = svc.ValidateArguments(new Transput[] { TestHelper.NumParameter });
            Assert.True(res.Status == StatusCode.Failed);
        }
        #endregion

        #region Func and expressions

        [Fact]
        public void InMemFactory_SimpleExpression_ShouldValidate()
        {
            IOperationService svc = OperationFactory.Create(() => 5, "GETFIVE");
            var goodInput = Transput.None;
            var res = svc.ValidateArguments(goodInput);
            Assert.True(res.Status == StatusCode.Ok);

            var badInput = TestHelper.NumParameter;
            res = svc.ValidateArguments(badInput);
            Assert.True(res.Status == StatusCode.Failed);
        }

        [Fact]
        public async void InMemFactory_UnaryExpression_ShouldExecute()
        {
            IOperationService svc = OperationFactory.Create(() => 5, "GETFIVE");
            var goodInput = Transput.None;
            var isItFive = await svc.ExecuteAsync(goodInput);

            Assert.True(5 == (int)isItFive[0].Value);
        }
        [Fact]
        public async void InMemFactory_InputOutputFunc_ShouldExecute()
        {
            IOperationService svc = OperationFactory.Create<double, double>(x => 5 * x, "GETFIVEBYFIVE");
            double orig = (double)TestHelper.NumParameter.Value;
            var goodInput = TestHelper.NumParameter;
            goodInput.Definition.Name = svc.Contract.InputDefinitions[0].Name;
            var isItFive = await svc.ExecuteAsync(goodInput);

            Assert.True(5 * orig == (double)isItFive[0].Value);
        }

        #endregion
    }
}
