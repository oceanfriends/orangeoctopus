using Benj.Common;
using OrangeOctopus.Operations.Process;
using OrangeOctopus.Operations.Contracts;
using OrangeOctopus.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Newtonsoft.Json;
using Xunit;

namespace OrangeOctopus.Operations.Tests
{
    public class OperationContractTests
    {
        [Fact]
        public void BaseValidContract_ShouldConstruct()
        {
            var id = Guid.NewGuid().ToString();
            var meta = new OperationMeta();

            TransputDefinition[] input = new TransputDefinition[]
            { TransputFactory.DefinitionFromType<string>("bloop") };

            TransputDefinition[] output = new TransputDefinition[]
            { TransputFactory.DefinitionFromType<string>("bloop") };

            var ctr = new OperationContract(
                    "TestContract",
                    input,
                    output,
                    OperationType.Expression,
                    new OperationMeta(),
                    id
                );

            Assert.True(ctr.Name == "TestContract");
            Assert.True(ctr.InputDefinitions.Length == 1);
            Assert.True(ctr.OutputDefinitions.Length == 1);

            Assert.True(ctr.InputDefinitions[0].Equivalent(TestHelper.StringDefinition));
            Assert.True(ctr.OutputDefinitions[0].Equivalent(TestHelper.StringDefinition));

        }

        [Fact]
        public void BaseContractWithNoTransputDefs_ShouldNotConstruct()
        {
            var id = Guid.NewGuid().ToString();
            var meta = new OperationMeta();

            Func<OperationContract> del = () => new OperationContract(
                    "TestContract",
                    new TransputDefinition[] { },
                    new TransputDefinition[] { },
                    OperationType.Expression,
                    new OperationMeta(),
                    id

                );
            Assert.ThrowsAny<ArgumentOutOfRangeException>(del);
        }

        [Fact]
        public void ValidExpressionOperationContract_ShouldConstruct()
        {
            Expression<Func<Transput[], Transput[]>> expr = inputs => new Transput[] { TransputFactory.CreateFrom(5, "Test") };
            var meta = new ExpressionOperationMeta(expr);

            TransputDefinition[] input = new TransputDefinition[]
            { TransputFactory.DefinitionFromType<string>("bloop") };

            TransputDefinition[] output = new TransputDefinition[]
            { TransputFactory.DefinitionFromType<int>("bloop") };

            var ctr = new ExpressionOperationContract("TestExpression", input, output, meta);

            Assert.True(ctr.Name == "TestExpression");
            Assert.True(ctr.InputDefinitions.Length == 1);
            Assert.True(ctr.OutputDefinitions.Length == 1);

            Assert.True(ctr.InputDefinitions[0].Equivalent(TestHelper.StringDefinition));
            Assert.True(ctr.OutputDefinitions[0].Equivalent(TestHelper.IntDefinition));

            var e = ctr.MetaFor as ExpressionOperationMeta;
            Assert.True(expr != null, "The expression metadata was not the correct type");

            var p = TransputFactory.CreateFrom("help", "bloop").ToArray();
            var res = e.ExpressionRepresentation.Compile().Invoke(p);
            Assert.True((int)res[0].Value == 5);

        }

        [Fact]
        public void ValidExpressionOperationContract_ShouldConstructFromSeed()
        {
            Expression<Func<Transput[], Transput[]>> expr = inputs => new Transput[] { TransputFactory.CreateFrom(5, "Test") };
            var meta = new ExpressionOperationMeta(expr);
            var meta2 = new OperationMeta();

            TransputDefinition[] input = new TransputDefinition[]
            { TransputFactory.DefinitionFromType<string>("bloop") };

            TransputDefinition[] output = new TransputDefinition[]
            { TransputFactory.DefinitionFromType<int>("bloop") };

            var ctrSeed = new OperationContract("TestExpression", input, output, OperationType.Expression, meta2);
            var ctr = new ExpressionOperationContract(ctrSeed, meta);

            Assert.True(ctr.Name == "TestExpression");
            Assert.True(ctr.InputDefinitions.Length == 1);
            Assert.True(ctr.OutputDefinitions.Length == 1);

            Assert.True(ctr.InputDefinitions[0].Equivalent(TestHelper.StringDefinition));
            Assert.True(ctr.OutputDefinitions[0].Equivalent(TestHelper.IntDefinition));

            var e = ctr.MetaFor as ExpressionOperationMeta;
            Assert.True(expr != null, "The expression metadata was not the correct type");

            var p = TransputFactory.CreateFrom("help", "bloop").ToArray();
            var res = e.ExpressionRepresentation.Compile().Invoke(p);
            Assert.True((int)res[0].Value == 5);

        }

        #region Serializing
        [Fact]
        public void SimpleContract_ShouldSerialize()
        {
            var settings = TestHelper.GetSerializerSettings();

            var id = Guid.NewGuid().ToString();
            var meta = new OperationMeta()
            {
                Endpoint = "www.checkit.com",
                ResourceUriBuilder = x => String.Format("www.checkit.com/{0}", x[0].Value)
            };

            TransputDefinition[] input = new TransputDefinition[]
            { TransputFactory.DefinitionFromType<string>("bloop") };

            TransputDefinition[] output = new TransputDefinition[]
            { TransputFactory.DefinitionFromType<string>("bloop") };

            var ctr = new OperationContract(
                    "TestContract",
                    input,
                    output,
                    OperationType.Expression,
                    meta,
                    id
                );

            string serialized = JsonConvert.SerializeObject(ctr, settings);
            var deserialized = JsonConvert.DeserializeObject<OperationContract>(serialized, settings);

            Assert.True(deserialized.Name == "TestContract");
            Assert.True(deserialized.Id == id);
            Assert.True(deserialized.InputDefinitions.Length == 1);
            Assert.True(deserialized.OutputDefinitions.Length == 1);

            Assert.True(deserialized.InputDefinitions[0].Equivalent(TestHelper.StringDefinition));
            Assert.True(deserialized.OutputDefinitions[0].Equivalent(TestHelper.StringDefinition));

            Assert.True(deserialized.Meta.Endpoint == "www.checkit.com");

            var del = deserialized.Meta.ResourceUriBuilder.Compile();
            var y = TransputFactory.CreateFrom(5, "bloop").ToArray();
            var str = del.Invoke(y);
            Assert.True(str == "www.checkit.com/5", $"expected www.checkit.com but actual was {str}");


        }

        [Fact]
        public void ExpressionContract_ShouldSerialize()
        {
            var settings = TestHelper.GetSerializerSettings();
            Expression<Func<Transput[], Transput[]>> expression = inputs => TransputFactory.CreateFrom((double)inputs[0].Value * 5, "OUT:TEST").ToArray();

            var id = Guid.NewGuid().ToString();
            var meta = new ExpressionOperationMeta(expression)
            {
                Endpoint = "www.checkit.com",
                ResourceUriBuilder = x => String.Format("www.checkit.com/{0}", x[0].Value)
            };

            TransputDefinition[] input = new TransputDefinition[]
            { TransputFactory.DefinitionFromType<double>("bloop") };

            TransputDefinition[] output = new TransputDefinition[]
            { TransputFactory.DefinitionFromType<string>("bloop") };

            var ctr = new ExpressionOperationContract(
                    "TestContract",
                    input,
                    output,
                    meta,
                    id
                );

            string serialized = JsonConvert.SerializeObject(ctr, settings);
            var deserialized = JsonConvert.DeserializeObject<ExpressionOperationContract>(serialized, settings);

            Assert.True(deserialized.Name == "TestContract");
            Assert.True(deserialized.Id == id);
            Assert.True(deserialized.InputDefinitions.Length == 1);
            Assert.True(deserialized.OutputDefinitions.Length == 1);

            Assert.True(deserialized.InputDefinitions[0].Equivalent(TestHelper.DoubleDefinition));
            Assert.True(deserialized.OutputDefinitions[0].Equivalent(TestHelper.StringDefinition));

            Assert.True(deserialized.Meta.Endpoint == "www.checkit.com");

            var del = deserialized.Meta.ResourceUriBuilder.Compile();
            var y = TransputFactory.CreateFrom(5d, "bloop").ToArray();
            var str = del.Invoke(y);
            Assert.True(str == "www.checkit.com/5", $"expected www.checkit.com but actual was {str}");

            ExpressionOperationMeta deserializedMeta  = (ExpressionOperationMeta)deserialized.MetaFor;
            var del2 = deserializedMeta.ExpressionRepresentation.Compile();
            var val = del2.Invoke(y);
            Assert.True((double)val[0].Value == 25);

        }

        [Fact]
        public void ExcelContract_ShouldSerialize()
        {
            var settings = TestHelper.GetSerializerSettings();
            var authProps = new AuthProperties()
            {
                Authority = "Ben",
                ApplicationId = "1234",
                ApplicationSecret = "MOTHERFUCKER",
                UserObjectId = "sdfas"
            };
            var mappings = new Dictionary<string, ExcelCell>()
            {
                ["ExcelInput1"] = new ExcelCell("Calculation", "A", 1),
                ["ExcelInput2"] = new ExcelCell("Calculation", "A", 2),
                ["ExcelOutputMultiplication"] = new ExcelCell("Calculation", "A", 3),
                ["ExcelOutputSum"] = new ExcelCell("Calculation", "A", 4),
            };

            var opMeta = new ExcelOperationMeta(mappings, authProps, "1", "123")
            {
                Endpoint = "www.checkit.com",
                ResourceUriBuilder = x => String.Format("www.checkit.com/{0}", x[0].Value),
            };

            var id = Guid.NewGuid().ToString();

            TransputDefinition[] input = new TransputDefinition[]
            { TransputFactory.DefinitionFromType<double>("bloop") };

            TransputDefinition[] output = new TransputDefinition[]
            { TransputFactory.DefinitionFromType<string>("bloop") };

            var ctr = new ExcelOperationContract
            (name: "TestContract", meta: opMeta,
                inputDefinitions: new TransputDefinition[] {
                    new TransputDefinition
                    {
                        Name = "ExcelInput1",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    },
                    new TransputDefinition
                    {
                        Name = "ExcelInput2",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    }
                },
                outputDefinitions: new TransputDefinition[] {
                    new TransputDefinition
                    {
                        Name = "ExcelOutputMultiplication",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    },
                    new TransputDefinition
                    {
                        Name = "ExcelOutputSum",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    }
                }, id: id);

            string serialized = JsonConvert.SerializeObject(ctr, settings);
            var deserialized = JsonConvert.DeserializeObject<ExcelOperationContract>(serialized, settings);

            Assert.True(deserialized.Name == "TestContract");
            Assert.True(deserialized.Id == id);
            Assert.True(deserialized.InputDefinitions.Length == 2);
            Assert.True(deserialized.OutputDefinitions.Length == 2);

            Assert.True(deserialized.InputDefinitions[0].Equivalent(TestHelper.DoubleDefinition));
            Assert.True(deserialized.OutputDefinitions[0].Equivalent(TestHelper.DoubleDefinition));
            Assert.True(deserialized.InputDefinitions[1].Equivalent(TestHelper.DoubleDefinition));
            Assert.True(deserialized.OutputDefinitions[1].Equivalent(TestHelper.DoubleDefinition));

            Assert.True(deserialized.Meta.Endpoint == "www.checkit.com");

            var del = deserialized.Meta.ResourceUriBuilder.Compile();
            var y = TransputFactory.CreateFrom(5d, "bloop").ToArray();
            var str = del.Invoke(y);
            Assert.True(str == "www.checkit.com/5", $"expected www.checkit.com but actual was {str}");

            ExcelOperationMeta deserializedMeta  = (ExcelOperationMeta)deserialized.MetaFor;
            Assert.True(deserializedMeta.DriveId == "1");
            Assert.True(deserializedMeta.WorkbookId == "123");
            Assert.True(deserializedMeta.AuthProperties.Authority == "Ben");
            Assert.True(deserializedMeta.AuthProperties.ApplicationId == "1234");
            Assert.True(deserializedMeta.AuthProperties.ApplicationSecret == "MOTHERFUCKER");
            Assert.True(deserializedMeta.AuthProperties.UserObjectId == "sdfas");

        }


        #endregion


    }
}
