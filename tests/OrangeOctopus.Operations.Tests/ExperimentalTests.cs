﻿using Aq.ExpressionJsonSerializer;
using Benj.Common.Data;
using Newtonsoft.Json;
using OrangeOctopus.Operations.Process;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace OrangeOctopus.Operations.Tests
{
    public class ExperimentalTests
    {

        private class NamedExpression
        {
            public string Name { get; set; }
            public LambdaExpression Expression { get; set; } //this is because the serializer can't handler abstracts.. this will be a problem if I can't figure it out
        }
        public void DoSomeShit()
        {
            Expression<Func<SimplePoco, bool>> expression = poco => poco.Int >= 50;
            var expr = new NamedExpression
            {
                Expression = expression,
                Name = "Test"
            };

            var settings = new JsonSerializerSettings();
            settings.Converters.Add(new ExpressionJsonConverter(Assembly.GetAssembly(typeof(ExperimentalTests))));
            var repo = new SimpleSerializingRepo(x => ((NamedExpression)x).Name, settings);
            //var repo = new SimpleSerializingRepo(x => "ben", settings);

            repo.Create(expr);

            var res = repo.Get<NamedExpression>("Test");
            Expression<Func<SimplePoco, bool>> expressionCopy = res.Payload.Expression as Expression<Func<SimplePoco, bool>>;
            //Expression<Func<SimplePoco, bool>> expressionCopy = res.Payload as Expression<Func<SimplePoco, bool>>;

            var predSvc = PredicateOperation<SimplePoco>.Create(expressionCopy, "FuckTheWorld");
            var svc1 = OperationFactory.Create<SimplePoco>(() => TestHelper.GetAPoco(), "MakeAPoco");
            var svc2 = OperationFactory.Create<SimplePoco>(poco => poco.Int *=2, "DoubleMyShit");

            var res1 = svc1.Execute(Transput.Empty);
            res1.Definition.Name = svc2.Contract.InputDefinitions[0].Name; //this needs to be handled more gracefully. maybe even by converntion....
            var res2 = svc2.Execute(res1);
            res2.Definition.Name = predSvc.Contract.InputDefinitions[0].Name; //this needs to be handled more gracefully. maybe even by converntion....

            var myEntity = res1.Value as SimplePoco;

            if (!predSvc.Execute(myEntity)) Assert.True(false);

            res1 = svc1.Execute(Transput.Empty);
            //manipulate some shit. should update by ref
            myEntity = res1.Value as SimplePoco;
            myEntity.Int = 20;

            res1.Definition.Name = svc2.Contract.InputDefinitions[0].Name; //this needs to be handled more gracefully. maybe even by converntion....
            res2 = svc2.Execute(res1);
            res2.Definition.Name = predSvc.Contract.InputDefinitions[0].Name; //this needs to be handled more gracefully. maybe even by converntion....
            if (predSvc.Execute(myEntity)) Assert.True(false);


        }
    }
}
