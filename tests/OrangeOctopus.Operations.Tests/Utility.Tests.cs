﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace OrangeOctopus.Operations.Tests
{
    public class UtilityTests
    {
        [Fact]
        public void GetArumentType_ShouldGetSimpleTypes()
        {
            Assert.True(Utility.DetermineStructureType<int>() == StructureType.Single);
            Assert.True(Utility.DetermineStructureType<bool>() == StructureType.Single);
            Assert.True(Utility.DetermineStructureType<double>() == StructureType.Single);
            Assert.True(Utility.DetermineStructureType<byte>() == StructureType.Single);
            Assert.True(Utility.DetermineStructureType<string>() == StructureType.Single);
            Assert.True(Utility.DetermineStructureType<DateTime>() == StructureType.Single);

        }
        [Fact]
        public void GetArumentType_ShouldGetSimpleRangeTypes()
        {
            Assert.True(Utility.DetermineStructureType<int[]>() == StructureType.Range);
            Assert.True(Utility.DetermineStructureType<bool[]>() == StructureType.Range);
            Assert.True(Utility.DetermineStructureType<double[]>() == StructureType.Range);
            Assert.True(Utility.DetermineStructureType<byte[]>() == StructureType.Range);
            Assert.True(Utility.DetermineStructureType<string[]>() == StructureType.Range);
            Assert.True(Utility.DetermineStructureType<DateTime[]>() == StructureType.Range);

        }
        [Fact]
        public void GetArumentType_ShouldGetComplexTypes()
        {
            Assert.True(Utility.DetermineStructureType<MyComplexObject>() == StructureType.Complex);
            Assert.True(Utility.DetermineStructureType<List<object>>() == StructureType.Complex);
            Assert.True(Utility.DetermineStructureType<IEnumerable<object>>() == StructureType.Complex);
            Assert.True(Utility.DetermineStructureType<Action>() == StructureType.Complex);
            Assert.True(Utility.DetermineStructureType<Object>() == StructureType.Complex);

        }
        [Fact]
        public void GetArumentType_ShouldGetComplexRangeTypes()
        {
            Assert.True(Utility.DetermineStructureType<MyComplexObject[]>() == StructureType.ComplexRange);
            Assert.True(Utility.DetermineStructureType<List<object>[]>() == StructureType.ComplexRange);
            Assert.True(Utility.DetermineStructureType<IEnumerable<object>[]>() == StructureType.ComplexRange);
            Assert.True(Utility.DetermineStructureType<Action[]>() == StructureType.ComplexRange);
            Assert.True(Utility.DetermineStructureType<Object[]>() == StructureType.ComplexRange);

        }

        private class MyComplexObject
        {
            string One;
            string Two;
            int Three;
            byte[] Four;
            DateTime Five;
        }
        
    }
}
