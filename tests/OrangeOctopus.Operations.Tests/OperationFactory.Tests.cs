using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xunit;
using Benj.Common;
using System.Linq.Expressions;
using OrangeOctopus.Operations.Contracts;
using OrangeOctopus.Operations.Process;
using OrangeOctopus.Shared;

namespace OrangeOctopus.Operations.Tests
{
    public class OperationFactoryTests 
    {
        private OperationFactory _instance;
        public OperationFactoryTests()
        {
            _instance = new OperationFactory();
            
        }
        [Fact]
        public void ShouldNotConstructInMemoryOps()
        {
            var contract = new OperationContract(
                    "TestContract",
                    TransputFactory.DefinitionFromType<string>("bloop").ToArray(),
                    TransputFactory.DefinitionFromType<string>("bloop").ToArray(),
                    OperationType.InMemory,
                    new OperationMeta(),
                    "ballin"
                    );

            var response = _instance.Create(contract);
            Assert.True(response.Status == StatusCode.Failed);
            
        }

        [Fact]
        public void ShouldNotConstructInvalidExpression()
        {
            var contract = new OperationContract(
                    "TestContract",
                    TransputFactory.DefinitionFromType<string>("bloop").ToArray(),
                    TransputFactory.DefinitionFromType<string>("bloop").ToArray(),
                    OperationType.Expression,
                    new OperationMeta(),
                    "ballin"
                    );

            var response = _instance.Create(contract);
            Assert.True(response.Status == StatusCode.Failed);
            
        }

        [Fact]
        public void ShouldConstructValidExpression()
        {
            Expression<Func<Transput[], Transput[]>> expr = inputs => new Transput[] { TransputFactory.CreateFrom(5, "Test") };
            var meta = new ExpressionOperationMeta(expr);
            TransputDefinition[] input = new TransputDefinition[]
            { TransputFactory.DefinitionFromType<string>("bloop") };
            TransputDefinition[] output = new TransputDefinition[]
            { TransputFactory.DefinitionFromType<int>("bloop") };
            var ctr = new ExpressionOperationContract("TestExpression", input, output, meta);

            var response = _instance.Create(ctr);
            Assert.True(response.Status == StatusCode.Ok, "The operation factory could not construct a valid expression operation");
            Assert.True(response.Payload != null, "The operation factory returned an OK response but did not have a valid object attached");

            //don't need to test the service itself here, since that is handled by the operation tests
            var svc = response.Payload;
            Assert.IsType<ExpressionOperation>(svc);
            
        }

        [Fact]
        public void ShouldConstructValidHttpGet()
        {
            var id = Guid.NewGuid().ToString();
            var meta = new OperationMeta()
            {
                Endpoint = "www.checkit.com",
                ResourceUriBuilder = x => String.Format("www.checkit.com/{0}", x[0].Value)
            };

            TransputDefinition[] input = new TransputDefinition[]
            { TransputFactory.DefinitionFromType<string>("bloop") };

            TransputDefinition[] output = new TransputDefinition[]
            { TransputFactory.DefinitionFromType<string>("bloop") };

            var ctr = new OperationContract(
                    "TestContract",
                    input,
                    output,
                    OperationType.HttpGet,
                    meta,
                    id
                );

            var response = _instance.Create(ctr);
            Assert.True(response.Status == StatusCode.Ok, "The operation factory could not construct a valid httpget  operation");
            Assert.True(response.Payload != null, "The operation factory returned an OK response but did not have a valid object attached");

            //don't need to test the service itself here, since that is handled by the operation tests
            var svc = response.Payload;
            Assert.IsType<BaseHttpGetOperation>(svc);
            
        }

        /// <summary>
        /// http get needs to have metadata defined for an endpoint. otherwise its useless!
        /// </summary>
        [Fact]
        public void ShouldNotConstructInvalidHttpGet()
        {
            var id = Guid.NewGuid().ToString();
            var meta = new OperationMeta();

            TransputDefinition[] input = new TransputDefinition[]
            { TransputFactory.DefinitionFromType<string>("bloop") };

            TransputDefinition[] output = new TransputDefinition[]
            { TransputFactory.DefinitionFromType<string>("bloop") };

            var ctr = new OperationContract(
                    "TestContract",
                    input,
                    output,
                    OperationType.HttpGet,
                    meta,
                    id
                );

            var response = _instance.Create(ctr);
            Assert.True(response.Status != StatusCode.Ok, "The factory returned a valid object when it should't have been able to");
            
        }

        [Fact]
        public void ShouldConstructValidExcel()
        {
            var authProps = new AuthProperties()
            {
                Authority = "Ben",
                ApplicationId = "1234",
                ApplicationSecret = "abc",
                UserObjectId = "sdfas"
            };

            var mappings = new Dictionary<string, ExcelCell>()
            {
                ["ExcelInput1"] = new ExcelCell("bloop", "A", 1),
                ["ExcelInput2"] = new ExcelCell("bloop", "A", 2),
                ["ExcelOutputMultiplication"] = new ExcelCell("bloop", "A", 3),
                ["ExcelOutputSum"] = new ExcelCell("bloop", "A", 4),
            };

            var opMeta = new ExcelOperationMeta(mappings, authProps, "1", "123") 
            {
                Endpoint = "www.checkit.com",
                ResourceUriBuilder = x => String.Format("www.checkit.com/{0}", x[0].Value)
            };

            var id = Guid.NewGuid().ToString();

            var ctr = new ExcelOperationContract
            (name: "TestContract", meta: opMeta,
                inputDefinitions: new TransputDefinition[] {
                    new TransputDefinition
                    {
                        Name = "ExcelInput1",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    },
                    new TransputDefinition
                    {
                        Name = "ExcelInput2",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    }
                },
                outputDefinitions: new TransputDefinition[] {
                    new TransputDefinition
                    {
                        Name = "ExcelOutputMultiplication",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    },
                    new TransputDefinition
                    {
                        Name = "ExcelOutputSum",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    }
                }, id: id);

            var response = _instance.Create(ctr);
            Assert.True(response.Status == StatusCode.Ok, "The operation factory could not construct a valid excel operation");
            Assert.True(response.Payload != null, "The operation factory returned an OK response but did not have a valid object attached");

            //don't need to test the service itself here, since that is handled by the operation tests
            var svc = response.Payload;
            Assert.IsType<ExcelOperation>(svc);
            
        }

        [Fact]
        public void ShouldNotConstructExcel_MissingAuth()
        {
            var authProps = new AuthProperties();

            var mappings = new Dictionary<string, ExcelCell>()
            {
                ["ExcelInput1"] = new ExcelCell("bloop", "A", 1),
                ["ExcelInput2"] = new ExcelCell("bloop", "A", 2),
                ["ExcelOutputMultiplication"] = new ExcelCell("bloop", "A", 3),
                ["ExcelOutputSum"] = new ExcelCell("bloop", "A", 4),
            };
            var opMeta = new ExcelOperationMeta(mappings, authProps, "1", "123") 
            {
                Endpoint = "www.checkit.com",
                ResourceUriBuilder = x => String.Format("www.checkit.com/{0}", x[0].Value)
            };

            var id = Guid.NewGuid().ToString();

            var ctr = new ExcelOperationContract
            (name: "TestContract", meta: opMeta,
                inputDefinitions: new TransputDefinition[] {
                    new TransputDefinition
                    {
                        Name = "ExcelInput1",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    },
                    new TransputDefinition
                    {
                        Name = "ExcelInput2",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    }
                },
                outputDefinitions: new TransputDefinition[] {
                    new TransputDefinition
                    {
                        Name = "ExcelOutputMultiplication",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    },
                    new TransputDefinition
                    {
                        Name = "ExcelOutputSum",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    }
                }, id: id);

            var response = _instance.Create(ctr);
            Assert.True(response.Status != StatusCode.Ok, "The operation factory could not construct a valid excel operation");
            Assert.True(response.Payload == null, "The operation factory returned an OK response but did not have a valid object attached");

        }

        [Fact]
        public void ShouldNotConstructExcel_MissingDriveId()
        {
            var authProps = new AuthProperties();

            var mappings = new Dictionary<string, ExcelCell>()
            {
                ["ExcelInput1"] = new ExcelCell("bloop", "A", 1),
                ["ExcelInput2"] = new ExcelCell("bloop", "A", 2),
                ["ExcelOutputMultiplication"] = new ExcelCell("bloop", "A", 3),
                ["ExcelOutputSum"] = new ExcelCell("bloop", "A", 4),
            };

            var opMeta = new ExcelOperationMeta(mappings,authProps,null,null) 
            {
                Endpoint = "www.checkit.com",
                ResourceUriBuilder = x => String.Format("www.checkit.com/{0}", x[0].Value)
            };

            var id = Guid.NewGuid().ToString();

            var ctr = new ExcelOperationContract
            (name: "TestContract", meta: opMeta,
                inputDefinitions: new TransputDefinition[] {
                    new TransputDefinition
                    {
                        Name = "ExcelInput1",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    },
                    new TransputDefinition
                    {
                        Name = "ExcelInput2",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    }
                },
                outputDefinitions: new TransputDefinition[] {
                    new TransputDefinition
                    {
                        Name = "ExcelOutputMultiplication",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    },
                    new TransputDefinition
                    {
                        Name = "ExcelOutputSum",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    }
                }, id: id);

            var response = _instance.Create(ctr);
            Assert.True(response.Status != StatusCode.Ok, "The operation factory could not construct a valid excel operation");
            Assert.True(response.Payload == null, "The operation factory returned an OK response but did not have a valid object attached");

        }

        [Fact]
        public void ShouldNotConstructExcel_NotExcelType()
        {
            var opMeta = new OperationMeta() 
            {
                Endpoint = "www.checkit.com",
                ResourceUriBuilder = x => String.Format("www.checkit.com/{0}", x[0].Value)
            };

            var id = Guid.NewGuid().ToString();

            var ctr = new OperationContract
            (name: "TestContract", meta: opMeta, opType: OperationType.Excel,
                inputDefinitions: new TransputDefinition[] {
                    new TransputDefinition
                    {
                        Name = "ExcelInput1",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    },
                    new TransputDefinition
                    {
                        Name = "ExcelInput2",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    }
                },
                outputDefinitions: new TransputDefinition[] {
                    new TransputDefinition
                    {
                        Name = "ExcelOutputMultiplication",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    },
                    new TransputDefinition
                    {
                        Name = "ExcelOutputSum",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    }
                }, id: id);

            var response = _instance.Create(ctr);
            Assert.True(response.Status != StatusCode.Ok, "The operation factory could not construct a valid excel operation");
            Assert.True(response.Payload == null, "The operation factory returned an OK response but did not have a valid object attached");
            
        }
    }
}
