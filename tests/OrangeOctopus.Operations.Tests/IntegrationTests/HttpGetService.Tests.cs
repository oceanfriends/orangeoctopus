﻿using Benj.Common.Entities;
using Newtonsoft.Json;
using OrangeOctopus.Operations;
using OrangeOctopus.Operations.Process;
using SuaveRestApi.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Benj.Calculation.Tests.IntegrationTests
{
    [Collection("WebServerCollection")]
    public class HttpGetServiceTests 
    {
        WebServerFixture _fixture;

        public HttpGetServiceTests(WebServerFixture fixture)
        {
            this._fixture = fixture;

        }
        [Fact]
        public async Task HttpGet_ShouldGetHelloWorldAsync()
        {
            var meta = new OperationMeta()
            {
                Description = "Hello world suave server",
                Endpoint = WebServerFixture.RootUrl,
                ResourceUriBuilder = null
            };

            var contract = new OperationContract("I can haz web server?", 
                new[] { TransputDefinition.Empty }, 
                TransputFactory.DefinitionFromType<string>("hi").ToArray(),
                OperationType.HttpGet,
                meta);
            var svc = new BaseHttpGetOperation(contract);
            var res = await svc.ExecuteAsync(Transput.Empty);
            Assert.True(res[0].Value is string);
            Assert.True((string)res[0].Value == "Hello World");


        }
        [Fact]
        public async Task HttpGet_ShouldGetPeopleAsync()
        {
            var meta = new OperationMeta()
            {
                Description = "Hello world suave server",
                Endpoint = WebServerFixture.RootUrl + "people",
                ResourceUriBuilder = null
            };

            var contract = new OperationContract("I can haz web server?",
                new[] { TransputDefinition.Empty },
                TransputFactory.DefinitionFromType<string>("hi").ToArray(),
                OperationType.HttpGet,
                meta);
            var svc = new BaseHttpGetOperation(contract);
            var res = await svc.ExecuteAsync(Transput.Empty);
            Assert.True(res[0].Value is string);
            List<Person> data = JsonConvert.DeserializeObject<List<Person>>((string)res[0].Value);
            Assert.True(data.Any());
        }
        [Fact]
        public async Task HttpStrongGet_ShouldGetPeopleAsync()
        {
            var svc = new HttpGetOperation<Empty, List<Person>>(x => WebServerFixture.RootUrl + "people", "I can haz web server?");
            List<Person> res = await svc.ExecuteAsync(new Empty());
            Assert.True(res.Any());
        }
        [Fact]
        public async Task HttpStrongParamGet_ShouldGetPeopleAsync()
        {
            var svc = new HttpGetOperation<Empty, List<Person>>(x => WebServerFixture.RootUrl + "people", "I can haz web server?");
            Transput<List<Person>> res = await svc.ExecuteAsync(Transput.Empty) ;
            Assert.True(res.Value.GetType() == typeof(List<Person>));
            Assert.True(res.Value.Any());
        }
        [Fact]
        public async Task HttpStrongGet_ShouldGetPersonAsync()
        {
            Expression<Func<Transput[], string>> uriBuilder = id => String.Format(WebServerFixture.RootUrl + "people/{0}", id[0].Value);
            var svc = new HttpGetOperation<int, Person>(uriBuilder, "I can haz web server?");
            Person res = await svc.ExecuteAsync(1);
            Assert.True(res != null);
            Assert.True(res.Id == 1) ;
        }
    }
}
