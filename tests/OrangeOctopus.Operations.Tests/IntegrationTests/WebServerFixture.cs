﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Benj.Calculation.Tests.IntegrationTests
{
    public class WebServerFixture : IDisposable
    {
        private bool KILL_SERVER_FOR_TEST = false;

        private System.Diagnostics.Process _server;
        public const string RootUrl = "http://localhost:8080/";
        private bool _haveServerHandle = false;

        public WebServerFixture()
        {
            var client = new HttpClient();
            try
            {
                var exists = client.GetAsync(RootUrl).GetAwaiter().GetResult();
                exists.EnsureSuccessStatusCode();
            }
            catch
            {
                _server = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = "cmd.exe";
                startInfo.Arguments = @"/C dotnet run --project ..\..\..\..\Benj.TestServer\Benj.TestServer.fsproj";
                if(IsLinux) 
                {
                    startInfo.FileName = "/bin/bash";
                    startInfo.Arguments = @"dotnet run --project ../../../../Benj.TestServer/Benj.TestServer.fsproj";
                }
                _server.StartInfo = startInfo;
                _server.Start();
                Thread.Sleep(1000);
                _haveServerHandle = true;
            }

        }
        public void Dispose()
        {
            if (KILL_SERVER_FOR_TEST)
            {
                if (_haveServerHandle) _server?.Kill();
                var processes = System.Diagnostics.Process.GetProcessesByName("dotnet");
                //foreach (var process in processes)
                //{
                //    process.Kill();
                //}

            }
        }

        public static bool IsLinux
        {
            get
            {
                int p = (int) Environment.OSVersion.Platform;
                return (p == 4) || (p == 6) || (p == 128);
            }
        }
    }

    [CollectionDefinition("WebServerCollection")]
    public class WebServerCollection : ICollectionFixture<WebServerFixture> { }
}
