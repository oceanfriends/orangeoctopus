﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations.Tests
{
    public class SimplePoco
    {
        public string String { get; set; }
        public DateTime DateTime { get; set; }
        public int Int { get; set; }
        public bool Bool { get; set; }

        public override bool Equals(object obj)
        {
            if (obj is SimplePoco other)
                return (other.String == this.String
                    && other.DateTime == this.DateTime
                    && other.Int == this.Int
                    && other.Bool == this.Bool);
            else return false;

        }

        public override int GetHashCode()
        {
            return this.String.GetHashCode() + this.DateTime.GetHashCode() + this.Int.GetHashCode() + this.Bool.GetHashCode();
        }
    }
}
