﻿using System;
using Aq.ExpressionJsonSerializer;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Reflection;
using Benj.Common;
using Xunit;
using OrangeOctopus.Operations.Contracts;

namespace OrangeOctopus.Operations.Tests
{
    public static class TestHelper
    {

        public static bool IsEmptyResponse(Transput res)
        {
            return res == Transput.None;
        }

        public static SimplePoco GetAPoco()
        {
            return new SimplePoco()
            {
                String = "s",
                DateTime = DateTime.UtcNow,
                Int = 46,
                Bool = true
            };
        }

        public static void AssertOk(ActivityResult res)
        {
            Assert.True(res.Status == StatusCode.Ok, res.Message + $" : {res.Exception}");
        }

        public static JsonSerializerSettings GetSerializerSettings()
        {
            var settings = new JsonSerializerSettings();
            settings.Converters.Add(new ExpressionJsonConverter(Assembly.GetAssembly(typeof(Transput))));
            return settings;
        }

        public static Transput NumParameter => new Transput(25d, DoubleDefinition);

        public static Transput StringParameter => new Transput("Hello", StringDefinition);

        public static TransputDefinition StringDefinition => new TransputDefinition() 
        {
            StructureType = StructureType.Single,
            TypeName = typeof(string).FullName,
            Name = "StringDefinition",
            Required = true,
            TypeCode = TypeCode.String
        };

        public static TransputDefinition IntDefinition => new TransputDefinition() 
        {
            StructureType = StructureType.Single,
            TypeName = typeof(int).FullName,
            Name = "IntDefinition",
            Required = true,
            TypeCode = TypeCode.Int32
        };

        public static TransputDefinition DoubleDefinition => new TransputDefinition() 
        {
            StructureType = StructureType.Single,
            TypeName = typeof(double).FullName,
            Name = "DoubleDefinition",
            Required = true,
            TypeCode = TypeCode.Double
        };

        public static (bool success, string why) ContractsEquivalent(OperationContract ctr, OperationContract second)
        {
            bool winning = true;
            winning = ctr.Name == second.Name;
            if (!winning) return (winning, "Name of contract and dto do not match");
            winning = ctr.Id == second.Id;
            if (!winning) return (winning, "Id of contract and dto do not match");
            winning = ctr.InputDefinitions.Length == second.InputDefinitions.Count();
            if (!winning) return (winning, "Number of input defs of contract and dto do not match");
            winning = ctr.OutputDefinitions.Length == second.OutputDefinitions.Count();
            if (!winning) return (winning, "Number of output defs of contract and dto do not match");
            winning = ctr.OperationType == second.OperationType;
            if (!winning) return (winning, "Operatoin type of contract and dto do not match");
            winning = ctr.Meta.Description == second.Meta.Description;
            if (!winning) return (winning, "Description of contract and dto do not match");
            winning = ctr.Meta.OperationId == second.Id;
            if (!winning) return (winning, "Operation meta of contract had an incorrect operation id");
            winning = ctr.Meta.Endpoint == second.Meta.Endpoint;
            if (!winning) return (winning, "Operation meta of contract had an incorrect endpoint");

            var inputDtoList = second.InputDefinitions.ToList();
            foreach (var transputDef in ctr.InputDefinitions)
            {
                var found = inputDtoList.SingleOrDefault(x => x.Name == transputDef.Name);
                if (found == null) return (false, $"Could not find a input dto for the transput def name {transputDef.Name}");
                (bool won, string why) = TransputDefsEquivalent(transputDef, found);
                if (!won) return (won, why);
            }

            return (winning, "");
        }

        public static (bool success, string why) TransputDefsEquivalent(TransputDefinition def, TransputDefinition second)
        {
            bool winning = true;
            winning = def.Name == second.Name;
            if (!winning) return (winning, "Name of transput def and dto do not match");
            winning = def.Required == second.Required;
            if (!winning) return (winning, "Required flag for transput def and dto do not match");
            winning = def.Description == second.Description;
            if (!winning) return (winning, "Description of transput def and dto do not match");
            winning = def.Name == second.Name;
            if (!winning) return (winning, "Name of transput def and dto do not match");
            winning = def.StructureType == second.StructureType;
            if (!winning) return (winning, "Structure type of transput def and dto do not match");
            winning = def.TypeName == second.TypeName;
            if (!winning) return (winning, "Type name of transput def and dto do not match");
            winning = def.TypeCode == second.TypeCode;
            if (!winning) return (winning, "Type code of transput def and dto do not match");
            return (winning, "");
        }
        public static (bool success, string why) ExcelContractsEquivalent(ExcelOperationContract ctr, ExcelOperationContract second)
        {
            (bool won, string error) = ContractsEquivalent(ctr, second);
            if (!won) return (won, error);
            return ExcelMetaEquivalent((ExcelOperationMeta)ctr.Meta, (ExcelOperationMeta)second.Meta);
        }

        public static (bool success, string why) ExcelMetaEquivalent(ExcelOperationMeta meta, ExcelOperationMeta second)
        {
            if (meta.AuthProperties.ApplicationId != second.AuthProperties.ApplicationId) return (false, "Auth application id did not match");
            if (meta.AuthProperties.ApplicationSecret != second.AuthProperties.ApplicationSecret) return (false, "Auth application secret did not match");
            if (meta.AuthProperties.Authority != second.AuthProperties.Authority) return (false, "Auth authority did not match");
            if (meta.AuthProperties.UserObjectId != second.AuthProperties.UserObjectId) return (false, "Auth user object id did not match");

            if (meta.DriveId != second.DriveId) return (false, "Drive id did not match");
            if (meta.WorkbookId != second.WorkbookId) return (false, "Workbook id did not match");

            foreach (var kvp in meta.ExcelParamMappings)
            {
                if (!second.ExcelParamMappings.ContainsKey(kvp.Key)) return (false, $"Cell mappings missing key {kvp.Key}");
                ExcelCell cell = second.ExcelParamMappings[kvp.Key];
                if (kvp.Value.Column != cell.Column) return (false, $"Column did not match for {kvp.Key}");
                if (kvp.Value.Row != cell.Row) return (false, $"Row did not match for {kvp.Key}");
                if (kvp.Value.Worksheet != cell.Worksheet) return (false, $"Worksheet did not match for {kvp.Key}");
            }
            return (true, "");
        }
    }
}
