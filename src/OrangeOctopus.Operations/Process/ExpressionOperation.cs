﻿using Benj.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using OrangeOctopus.Operations.Contracts;

namespace OrangeOctopus.Operations.Process
{
    public class ExpressionOperation : IOperationService
    {
        public ExpressionOperationContract _contract {get; protected set;}

        protected ExpressionOperation() { }
        public ExpressionOperation(ExpressionOperationContract contract, InMemValidator validator = null)
        {
            _contract = contract;
            var meta = (ExpressionOperationMeta)contract.MetaFor;
            _compiledDel = meta.ExpressionRepresentation.Compile();
            _executor = inputs => _compiledDel(inputs);
            if (validator != null) _validator = validator;
            else _validator = inputs =>Validation.BasicNamedValidator(inputs, Contract.InputDefinitions);
        }

        public ExpressionOperation (string name,
            Expression<Func<Transput[], Transput[]>> expression,
            TransputDefinition[] inputDefs,
            TransputDefinition[] outputDef,
            InMemValidator validator = null,
            string description = "")
        {
            var meta = new ExpressionOperationMeta(expression)
            {
                Description = description
            };
            _contract = new ExpressionOperationContract(name, inputDefs, outputDef, meta);
            _compiledDel = expression.Compile();
            _executor = inputs => _compiledDel(inputs);
            if (validator != null) _validator = validator;
            else _validator = inputs =>Validation.BasicNamedValidator(inputs, Contract.InputDefinitions);
        }

        private readonly Func<Transput[], Transput[]> _compiledDel;
        protected readonly InMemExecutor _executor;
        protected readonly InMemValidator _validator;

        public string Name => Contract.Name;

        public OperationContract Contract => _contract;

        public virtual Transput[] Execute(params Transput[] inputs)
        {
            return _executor(inputs);
        }

        public virtual ActivityResult ValidateArguments(params Transput[] inputs)
        {
            return _validator(inputs);
        }

        public  Task<Transput[]> ExecuteAsync(params Transput[] inputs)
        {
            var res =Execute(inputs);
            return Task.FromResult(res.ToArray());
        }

        public static implicit operator InMemoryOperation(ExpressionOperation src)
        {
            var modContract = new OperationContract(src.Contract.Name, src.Contract.InputDefinitions, 
                src.Contract.OutputDefinitions, OperationType.InMemory, src.Contract.Meta);

            return new InMemoryOperation(src._executor, src._validator, modContract);
        }
        
    }
}
