﻿using Calculation.Helpers;
using System;
using System.Linq;
using System.Threading.Tasks;
using Benj.Common;
using OrangeOctopus.Operations.Contracts;
using OrangeOctopus.Shared;

namespace OrangeOctopus.Operations.Process
{
    public class ExcelOperation : IOperationService
    {
        // Excel graph endpoint url
        private const string baseExcelUrlTEMP = "https://graph.microsoft.com/v1.0";

        // Excel access token
        private ExcelAuthService _authService;
        private string _accessToken;
        private readonly AuthProperties _authProperties;
        private readonly string _driveId;
        private readonly string _workbookId;

        private ExcelOperationContract _opDef;

        public ExcelOperation(ExcelOperationContract operationDefinition, ExcelAuthService authService)
        {
            _opDef = operationDefinition;
            _authService = authService;
            var meta = (ExcelOperationMeta)operationDefinition.MetaFor;
            _authProperties = meta.AuthProperties;
            _driveId = meta.DriveId;
            _workbookId = meta.WorkbookId;
        }

        public string Name => Contract.Name;

        public OperationContract Contract => _opDef;

        public virtual async Task<Transput[]> ExecuteAsync(params Transput[] inputs)
        {
            var validationRes = ValidateArguments(inputs);

            // Get access token
            if(this._accessToken == null)
            {
                this._accessToken = await _authService.GetUserAccessToken(_authProperties);
            }

            // Get session
            var sessionId = await ExcelHelpers.CreateSession(baseExcelUrlTEMP, _driveId,  _workbookId, false, _accessToken);

            // Set given inputs
            await Task.WhenAll(inputs.Select(async input =>
            {
                if(input.Definition.StructureType == StructureType.Single)
                {
                    TransputDefinition def = _opDef.InputDefinitions.Single(x => x.Name == input.Definition.Name);
                    ExcelCell cell = ((ExcelOperationMeta)_opDef.MetaFor).ExcelParamMappings[def.Name];
                    await ExcelHelpers.SetCellValue(cell.Address, input.Value.ToString(), baseExcelUrlTEMP, _driveId, _workbookId, _accessToken, sessionId);
                    return;
                }
                else
                {
                    throw new InvalidOperationException("Input structure not supported.");
                }
            }));

            // Get all outputs
            var results = await Task.WhenAll(_opDef.OutputDefinitions.Select(async output =>
            {
                if (output.StructureType != StructureType.Single) throw new InvalidOperationException("Output structure not supported.");
                //ANDREW TODO
                //get the excel cell we are mapped to for the given input. matches on name
                ExcelCell cell = ((ExcelOperationMeta)_opDef.MetaFor).ExcelParamMappings[output.Name];
                var result = await ExcelHelpers.GetRangeValue(cell.Address, baseExcelUrlTEMP, _driveId, _workbookId, _accessToken, sessionId);
                return new Transput(result, output);
            }));

            return results;
        }

        public ActivityResult ValidateArguments(params Transput[] inputs)
        {
            //TODO excel validation delegate
            return Validation.BasicNamedValidator(inputs, _opDef.InputDefinitions);
        }
    }
}
