﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Benj.Common;
using System.Net.Http;
using System.Runtime.ExceptionServices;
using Newtonsoft.Json;
using System.Linq.Expressions;

namespace OrangeOctopus.Operations.Process
{

    public class HttpGetOperation<TInput, TOutput> : BaseHttpGetOperation, IStrongOperationService<TInput, TOutput>
    {
        public HttpGetOperation(Expression<Func<Transput[], string>> uriGetFactory, string name, string desciption = null)
        {
            var meta = new OperationMeta()
            {
                Description = desciption,
                ResourceUriBuilder = uriGetFactory,
            };
            Contract = new OperationContract(name, new[] { TransputFactory.DefinitionFromType<TInput>("IN:" + name) },
                TransputFactory.DefinitionFromType<TOutput>("OUT:"+name).ToArray(), OperationType.HttpGet, meta);
            _uriBuilder = Contract.Meta.ResourceUriBuilder?.Compile();
        }

        public virtual async Task<Transput<TOutput>> ExecuteAsync(Transput<TInput> inputs)
        {
            Transput[] baseParam = await ExecuteAsync(new Transput[] { inputs });
            var rtn = Transput<TOutput>.FromTransput(baseParam[0]);
            if (rtn.Status == StatusCode.Exception) ExceptionDispatchInfo.Capture(rtn.Exception).Throw();
            if (rtn.Status == StatusCode.Failed) throw new InvalidParamatersException("Could not convert the operation to a strongly typed parameter");
            return rtn.Payload;
        }

        public override async Task<Transput[]> ExecuteAsync(params Transput[] inputs)
        {
            var stringResult = await base.ExecuteAsync(inputs);
            //we inherit from the base, which we know to be interacting with the http endpoint and receiving a string response
            string raw = (string)stringResult[0].Value;
            TOutput retrieved = JsonConvert.DeserializeObject<TOutput>(raw);
            var rtn = TransputFactory.CreateFrom(retrieved, this.Contract.OutputDefinitions[0]);
            return new Transput[] { rtn };
        }

        public virtual async Task<TOutput> ExecuteAsync(TInput input)
        {
            StrongParameter<TOutput> res = await ExecuteAsync(TransputFactory.CreateFrom(input, this.Contract.InputDefinitions[0]));
            return res.Value;
        }

    }
}
