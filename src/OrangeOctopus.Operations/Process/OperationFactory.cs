﻿using Benj.Common;
using OrangeOctopus.Operations.Contracts;
using OrangeOctopus.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations.Process
{
    public class OperationFactory : IOperationFactory
    {

        public ActivityResult<IOperationService> Create(OperationContract contract) 
        {

            switch (contract.OperationType)
            {
                case OperationType.InMemory:
                    return Utility.Failure<IOperationService>("You cannot create an instance of an in mememory service from an anonymous contract");

                case OperationType.Expression:
                    var expressContract = contract as ExpressionOperationContract;
                    if(expressContract == null) return Utility.Failure<IOperationService>("The contract provided could not be converted to an expression contract");
                    try {
                        var op = new ExpressionOperation(expressContract);
                        return new ActivityResult<IOperationService>(op);
                    }
                    catch(Exception ex) {
                        return new ActivityResult<IOperationService>(ex);
                    }

                case OperationType.Input:
                    throw new NotImplementedException();

                case OperationType.HttpGet:
                    if(String.IsNullOrWhiteSpace(contract.Meta.Endpoint) 
                            && contract.Meta.ResourceUriBuilder == null) return Utility.Failure<IOperationService>("There was no endpoint defined on the contract, so an HTTP operation cannot be constructed");
                    try {
                        BaseHttpGetOperation op;
                        if(!contract.IsSingleOutput<string>())
                        {
                            op = new BaseHttpGetOperation(contract, true);
                            //TODO we need to strong type this bitch with reflection 
                            return new ActivityResult<IOperationService>(op);

                        }
                        op = new BaseHttpGetOperation(contract);
                        return new ActivityResult<IOperationService>(op);
                    }
                    catch(Exception ex) {
                        return new ActivityResult<IOperationService>(ex);
                    }

                case OperationType.HttpPost:
                    throw new NotImplementedException();

                case OperationType.Excel:
                    var excelDefinition = contract as ExcelOperationContract;
                    if(excelDefinition == null) return Utility.Failure<IOperationService>("The contract provided could not be converted to an excel contract");
                    try {

                        var excelMeta = (ExcelOperationMeta)excelDefinition.MetaFor;
                        if(String.IsNullOrWhiteSpace(excelMeta.AuthProperties?.ApplicationId) || String.IsNullOrWhiteSpace(excelMeta.AuthProperties?.ApplicationSecret))
                            return Utility.Failure<IOperationService>("Auth properties were not populated on the excel contract");
                        if(String.IsNullOrWhiteSpace(excelMeta.DriveId) || String.IsNullOrWhiteSpace(excelMeta.DriveId))
                            return Utility.Failure<IOperationService>("The operation contract was missing drive id or workbook id properties");

                        var excelSvc = new ExcelOperation(excelDefinition, new ExcelAuthService());
                        return new ActivityResult<IOperationService>(excelSvc);
                    }
                    catch(Exception ex) {
                        return new ActivityResult<IOperationService>(ex);
                    }

                case OperationType.ConsoleInput:
                    var outDefn = contract as OutputOnlyOperationContract;
                    if(outDefn == null) return Utility.Failure<IOperationService>("The contract provided could not be converted to a console input contract");
                    try {
                        var svc = new ConsoleInputOperation(outDefn);
                        return new ActivityResult<IOperationService>(svc);
                    }
                    catch(Exception ex) {
                        return new ActivityResult<IOperationService>(ex);
                    }

                case OperationType.ConsoleOutput:
                    var inDefn = contract as InputOnlyOperationContract;
                    if(inDefn == null) return Utility.Failure<IOperationService>("The contract provided could not be converted to a console output contract");
                    try {
                        var svc = new ConsoleOutputOperation(inDefn);
                        return new ActivityResult<IOperationService>(svc);
                    }
                    catch(Exception ex) {
                        return new ActivityResult<IOperationService>(ex);
                    }

                default:
                    return Utility.Failure<IOperationService>("You requested an unsupported type!");
            }

        }

        public ActivityResult<IOperationService> Create(OperationContract contract, OperationMeta meta)  
        {
            switch (contract.OperationType)
            {
                case OperationType.InMemory:
                    return Utility.Failure<IOperationService>("You cannot create an instance of an in mememory service from an anonymous contract");
                case OperationType.Expression:
                    var expressMeta = meta as ExpressionOperationMeta;
                    if(expressMeta == null) return Utility.Failure<IOperationService>("The contract provided could not be converted to an expression contract");
                    try {
                        var exprContract = new ExpressionOperationContract(contract, expressMeta);
                        return Create(exprContract);
                    }
                    catch(Exception ex) {
                        return new ActivityResult<IOperationService>(ex);
                    }

                case OperationType.Input:
                    throw new NotImplementedException();
                case OperationType.HttpGet:
                    return Create(contract);
                case OperationType.HttpPost:
                    throw new NotImplementedException();
                case OperationType.Excel:
                    var excelMeta = meta as ExcelOperationMeta;
                    if(excelMeta == null) return Utility.Failure<IOperationService>("The contract provided could not be converted to an excel contract");
                    try {
                        var excelContract = new ExcelOperationContract(contract, excelMeta);
                        return Create(excelContract);
                    }
                    catch(Exception ex) {
                        return new ActivityResult<IOperationService>(ex);
                    }

                case OperationType.ConsoleInput:
                    return Create(contract);
                case OperationType.ConsoleOutput:
                    return Create(contract);
                default:
                    return Utility.Failure<IOperationService>("You requested an unsupported type!");
            }

        }

        public static InMemoryOperation Create(Action action, string name)
        {
            var meta = new OperationMeta()
            {
                Description = "In memory anonymous calculation",
                Endpoint = ""
            };
            var contract = new OperationContract(name, TransputDefinition.EmptyArray, TransputDefinition.EmptyArray, OperationType.InMemory, meta);

            InMemValidator validator = inputs => Validation.BasicNamedValidator(inputs, contract.InputDefinitions);

            InMemExecutor executor = new InMemExecutor(inputs =>
            {
                if (validator(inputs).Status != StatusCode.Ok)
                    throw new InvalidParamatersException("You cannot pass parameters to an anonymous action");
                action();
                return Transput.None.ToArray();
            });

            return new InMemoryOperation(executor, validator, contract);
        }

        public static InMemoryOperation Create<T>(Action<T> action, string name, string description = null)
        {
            var meta = new OperationMeta()
            {
                Description = description ?? $"In memory anonymous calculation id {Guid.NewGuid()}",
                Endpoint = ""
            };

            var inputDef = TransputFactory.DefinitionFromType<T>(name + "-input");
            var contract = new OperationContract(name, new[] { inputDef }, TransputDefinition.EmptyArray, OperationType.InMemory, meta);


            InMemValidator validator = inputs => Validation.BasicNamedValidator(inputs, contract.InputDefinitions);

            InMemExecutor executor = new InMemExecutor(inputs =>
            {
                if (validator(inputs).Status != StatusCode.Ok)
                    throw new InvalidParamatersException("The input parameter you provided does not match the contract");
                action((T)inputs[0].Value);
                return Transput.NoneArray;
            });

            return new InMemoryOperation(executor, validator, contract);
        }

        public static InMemoryOperation Create<T>(Expression<Func<T>> action, string name, string description = null)
        {
            var meta = new OperationMeta()
            {
                Description = description ?? $"In memory anonymous calculation id {Guid.NewGuid()}",
                Endpoint = ""
            };

            var outputDef = TransputFactory.DefinitionFromType<T>(name + "-input");
            var contract = new OperationContract(name, TransputDefinition.EmptyArray, outputDef.ToArray(), OperationType.InMemory, meta);

            InMemValidator validator = inputs => Validation.BasicNamedValidator(inputs, contract.InputDefinitions);

            var compiled = action.Compile();

            InMemExecutor executor = new InMemExecutor(inputs =>
            {
                if (validator(inputs).Status != StatusCode.Ok)
                    throw new InvalidParamatersException("The input parameter you provided does not match the contract");
                T res = compiled();
                return TransputFactory.CreateFrom(res, name+"input").ToArray();
            });

            return new InMemoryOperation(executor, validator, contract);
        }

        public static InMemoryOperation Create<TInput, TOutput>(Expression<Func<TInput, TOutput>> action, string name, string description = null)
        {
            var meta = new OperationMeta()
            {
                Description = description ?? $"In memory anonymous calculation id {Guid.NewGuid()}",
                Endpoint = ""
            };

            var outputDef = TransputFactory.DefinitionFromType<TOutput>("OUT:"+name);
            var inputDef = TransputFactory.DefinitionFromType<TInput>("IN:"+name);
            var contract = new OperationContract(name, new[] { inputDef }, outputDef.ToArray(), OperationType.InMemory, meta);

            InMemValidator validator = inputs => Validation.BasicNamedValidator(inputs, contract.InputDefinitions);

            var compiled = action.Compile();

            InMemExecutor executor = new InMemExecutor(inputs =>
            {
                if (validator(inputs).Status != StatusCode.Ok)
                    throw new InvalidParamatersException("The input parameter you provided does not match the contract");
                TOutput res = compiled((TInput)inputs[0].Value);
                return TransputFactory.CreateFrom(res, contract.OutputDefinitions[0]).ToArray();
            });

            return new InMemoryOperation(executor, validator, contract);
        }

        public static HttpGetOperation<TInput, TOutput> FromBaseHttpService<TInput, TOutput>(BaseHttpGetOperation baseGet)
        {
            return new HttpGetOperation<TInput, TOutput>(baseGet.Contract.Meta.ResourceUriBuilder, baseGet.Name, baseGet.Contract.Meta.Description);
        }


    }

}
