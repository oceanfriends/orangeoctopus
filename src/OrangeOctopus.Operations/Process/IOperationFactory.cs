using Benj.Common;
using OrangeOctopus.Operations.Contracts;
using OrangeOctopus.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations.Process
{
    public interface IOperationFactory 
    {
        ActivityResult<IOperationService> Create(OperationContract contract);
        ActivityResult<IOperationService> Create(OperationContract contract, OperationMeta meta);
    }
}
