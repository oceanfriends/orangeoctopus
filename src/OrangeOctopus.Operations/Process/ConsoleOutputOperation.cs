﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OrangeOctopus.Operations.Contracts;
using System.Linq;
using Benj.Common;

namespace OrangeOctopus.Operations.Process
{
    public class ConsoleOutputOperation : IOperationService
    {

        public ConsoleOutputOperation(InputOnlyOperationContract operationDefinition)
        {
            this.Contract = operationDefinition;
        }

        public string Name => Contract.Name;

        public OperationContract Contract { get; private set; }

        public async Task<Transput[]> ExecuteAsync(params Transput[] inputs)
        {
            // TODO: compare parameters against expected definitions

            // Get value from user for each output
            await Task.WhenAll(inputs.Select(async input =>
            {
                await Task.Run(() => Console.WriteLine("Output for " + input.Definition.Name + ": " + input.Value.ToString()));
                return;
            }));

            return null;
        }

        public ActivityResult ValidateArguments(params Transput[] inputs)
        {
            return Validation.BasicNamedValidator(inputs, Contract.InputDefinitions);
        }
    }
}
