﻿using System;
using System.Threading.Tasks;
using Benj.Common;

namespace OrangeOctopus.Operations.Process
{
    public class InMemoryOperation : IOperationService
    {
        public InMemoryOperation(InMemExecutor whatToDo, InMemValidator validator, OperationContract contract)
        {
            _executor = whatToDo;
            _validator = validator;
            Contract = contract;
        }
        protected InMemoryOperation() { }
        private readonly InMemExecutor _executor;
        private readonly InMemValidator _validator;

        public string Name => Contract.Name;

        public OperationContract Contract { get; protected set; }

        public virtual Transput Execute(params Transput[] inputs)
        {
            return _executor(inputs)[0];
        }

        public virtual ActivityResult ValidateArguments(params Transput[] inputs)
        {
            return _validator(inputs);
        }

        public Task<Transput[]> ExecuteAsync(params Transput[] inputs)
        {
            var res = Execute(inputs);
            return Task.FromResult(res.ToArray());
        }
    }
}
