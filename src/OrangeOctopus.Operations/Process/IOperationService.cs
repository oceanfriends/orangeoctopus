﻿using Benj.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations
{
    public interface IOperationService
    {
        string Name { get;  }
        OperationContract Contract {get; }
        Task<Transput[]> ExecuteAsync(params Transput[] inputs);
        ActivityResult ValidateArguments(params Transput[] inputs);
    }
}
