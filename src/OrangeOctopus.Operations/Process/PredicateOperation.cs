﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Benj.Common;
using System.Linq.Expressions;
using OrangeOctopus.Operations.Contracts;

namespace OrangeOctopus.Operations.Process
{
    public class PredicateOperation<T> : ExpressionOperation
    {
        private PredicateOperation()
        { }

        public static PredicateOperation<T> Create(Expression<Func<T, bool>> predicate, string name)
        {
            //ugly ass shit
            Expression<Func<Transput[], Transput[]>> expression =
                inputs => TransputFactory.CreateFrom(predicate.Compile()((T)inputs[0].Value), $"IN:{name}").ToArray();
            var meta = new ExpressionOperationMeta(expression);
            var contract = new ExpressionOperationContract(name,
                new[] { TransputFactory.DefinitionFromType<T>("IN:" + name) },
                TransputFactory.DefinitionFromType<bool>("OUT:" + name).ToArray(),
                meta);
            var instance = new PredicateOperation<T>
            { };

            instance._expression = predicate;
            instance._contract = contract;
            instance._del = new Predicate<T>(predicate.Compile());
            return instance;
        }
        private Expression<Func<T, bool>> _expression;
        private Predicate<T> _del;

        public override Transput[] Execute(params Transput[] inputs)
        {
            if (ValidateArguments(inputs).Status != StatusCode.Ok)
                throw new InvalidParamatersException("The input parameter you provided does not match the contract");
            bool res = _del((T)inputs[0].Value);
            return new Transput[] { TransputFactory.CreateFrom(res, this.Contract.OutputDefinitions[0]) };
        }

        public bool Execute(T input)
        {
            var param = TransputFactory.CreateFrom(input, "IN:" + this.Name);
            var rtn = this.Execute(param);
            return (bool)rtn[0].Value; //throw here
        }

        public override ActivityResult ValidateArguments(params Transput[] inputs)
        {
            return Validation.BasicNamedValidator(inputs, this.Contract.InputDefinitions);
        }
    }
}
