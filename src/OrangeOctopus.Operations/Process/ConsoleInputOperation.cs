﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OrangeOctopus.Operations.Contracts;
using System.Linq;
using Benj.Common;

namespace OrangeOctopus.Operations.Process
{
    public class ConsoleInputOperation : IOperationService
    {
        public ConsoleInputOperation(OutputOnlyOperationContract contract)
        {
            // TODO - check that the structure type for all defs is simple
            this.Contract = contract;
        }

        public string Name => Contract.Name;

        public OperationContract Contract {get; protected set;}

        public async Task<Transput[]> ExecuteAsync(params Transput[] inputs)
        {
            // Get value from user for each output
            return await Task.Run(() => {
                var x = new List<Transput>();
                foreach (var defn in Contract.OutputDefinitions)
                {
                    Console.WriteLine("Please input a value for: " + defn.Name);
                    var input = Console.ReadLine();
                    x.Add(new Transput(input, defn));
                }
                return x.ToArray();
            });
        }

        public ActivityResult ValidateArguments(params Transput[] inputs)
        {
            return Validation.BasicNamedValidator(inputs, Contract.InputDefinitions);
        }

    }
}
