﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations
{
    public interface IStrongOperationService<TInput, TOutput> : IOperationService
    {
        Task<Transput<TOutput>> ExecuteAsync(Transput<TInput> inputs);
        Task<TOutput> ExecuteAsync(TInput input);
    }

}
