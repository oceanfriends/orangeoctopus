﻿using Benj.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations.Process
{
    /// <summary>
    /// We only work with json. Should add some validations
    /// </summary>
    public class BaseHttpGetOperation : IOperationService
    {
        protected static HttpClient _client = new HttpClient();
        public BaseHttpGetOperation(OperationContract contract, bool forceContractReturnToString = false)
        {
            if(contract == null) throw new ArgumentNullException(nameof(contract), "Operation contract cannot be null");
            this.Contract = contract;
            if (forceContractReturnToString)
            {
                //TODO add validations to handle multiple return types now as per ANDREW SULLIVAN
                this.Contract.OutputDefinitions = TransputFactory.DefinitionFromType<string>(contract.OutputDefinitions[0].Name).ToArray();
            }
            //TODO add validations to handle multiple return types now as per ANDREW SULLIVAN
            if(!Contract.OutputDefinitions[0].StructureType.Equals(StructureType.Single)
                || Contract.OutputDefinitions[0].TypeCode != TypeCode.String)
            {
                throw new ArgumentOutOfRangeException("Generic http calculation services can only return strings. It is the responsibility of the consumer to deserialize the string response");
            }
            if (Contract.Meta?.Endpoint == null && Contract.Meta?.ResourceUriBuilder == null)
                throw new ArgumentNullException(nameof(Contract.Meta), "You must provide a endpoint or endpoint builder for http requests");

            _uriBuilder = Contract.Meta.ResourceUriBuilder?.Compile() ?? new Func<Transput[], string>(inputs => Contract.Meta.Endpoint);
        }
        protected BaseHttpGetOperation()
        { }

        protected Func<Transput[], string> _uriBuilder;

        public string Name => Contract.Name;

        public OperationContract Contract { get; protected set; }


        public virtual ActivityResult ValidateArguments(params Transput[] inputs)
        {
            return Validation.BasicNamedValidator(inputs, Contract.InputDefinitions);
        }

        public async virtual Task<Transput[]> ExecuteAsync(params Transput[] inputs)
        {
            HttpResponseMessage resp = await _client.GetAsync(new Uri(_uriBuilder(inputs)));
            resp.EnsureSuccessStatusCode();
            string content = await resp.Content.ReadAsStringAsync();
            //need to create out param based on name, not contract def'n because the contract is not protected from deriving classes
            var outParam = TransputFactory.CreateFrom(content, Contract.OutputDefinitions[0].Name);
            return new Transput[] { outParam };
        }
    }
}
