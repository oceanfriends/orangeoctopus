﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Benj.Common;
using Benj.Common.Entities;

namespace OrangeOctopus.Operations
{
    public class Utility
    {
        public static StructureType DetermineStructureType<T>()
        {
            var t = typeof(T);
            if (t.IsArray)
            {
                var elementType = t.GetElementType();
                TypeCode code = Type.GetTypeCode(elementType);
                if (code == TypeCode.Object)
                    return StructureType.ComplexRange;
                else return StructureType.Range;
            }
            else
            {
                TypeCode code = Type.GetTypeCode(t);
                if (code == TypeCode.Object)
                    return StructureType.Complex;
                else return StructureType.Single;
            }
        }

        public static ActivityResult<T> Failure<T>(string message) 
        {
            var rtn = new ActivityResult<T>(default(T), StatusCode.Failed);
            rtn.Message = message;
            return rtn;
        }
        public static ActivityResult<T> Error<T>(Exception ex, string message) 
        {
            var rtn = new ActivityResult<T>(ex);
            rtn.Message = message;
            return rtn;
        }

    }
}
