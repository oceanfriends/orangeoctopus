﻿using Benj.Common;
using Benj.Common.Data;
using System.Collections.Generic;

namespace OrangeOctopus.Operations.Persistance
{
    public interface IOperationRegistry : ICanCrud<IOperationService>, ICanCrud<OperationContract>
    {
        ActivityResult<IEnumerable<IOperationService>> AllForUser(string userId);
        ActivityResult<IEnumerable<OperationContract>> Search(ISearchSpec<OperationContract> spec, SearchOption options = null);
    }
}
