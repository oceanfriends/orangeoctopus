﻿using Benj.Common.Data;
using System;
using System.Collections.Generic;
using Benj.Common;
using Benj.Common.Entities;
using OrangeOctopus.Operations.Process;
using OrangeOctopus.Operations.Contracts;
using OrangeOctopus.Shared;

namespace OrangeOctopus.Operations.Persistance
{
    public class OperationRegistry : IOperationRegistry
    {
        private readonly ICanCrud _repo;
        private readonly IOperationFactory _operationFactory;
        private static Dictionary<string, InMemoryOperation> _inMemorySvcCache = new Dictionary<string, InMemoryOperation>();
        public OperationRegistry(ICanCrud repo, IOperationFactory operationFactory)
        {
            _repo = repo;
            _operationFactory = operationFactory;
        }

        private ActivityResult Validate(IOperationService svc)
        {
            try
            {
                if (svc?.Contract == null)
                    throw new ArgumentNullException(nameof(svc.Contract));
                return new ActivityResult(StatusCode.Ok);
            }
            catch (Exception ex)
            {
                return new ActivityResult(ex);
            }

        }
        public IAtomicOperation BeginAtomicOperation()
        {
            return _repo.BeginAtomicOperation();
        }

        public ActivityResult<IOperationService> Create(IOperationService toSave)
        {
            var v = Validate(toSave);
            if (!v.IsOk()) return new ActivityResult<IOperationService>(v, null);
            //one off case for in memory calcs
            if (toSave.Contract.OperationType == OperationType.InMemory)
            {
                _inMemorySvcCache[toSave.Name] = (InMemoryOperation)toSave;
                return new ActivityResult<IOperationService>(toSave, StatusCode.Ok);
            }

            var savedContract = Create(toSave.Contract);
            if (!savedContract.IsOk()) return new ActivityResult<IOperationService>(savedContract);
            return _operationFactory.Create(savedContract.Payload);
        }

        public ActivityResult Delete(string key) => ((ICanCrud<OperationContract>)this).Delete(key);

        public ActivityResult<IOperationService> Get(string key)
        {
            if (_inMemorySvcCache.ContainsKey(key)) return new ActivityResult<IOperationService>(_inMemorySvcCache[key], StatusCode.Ok);
            var got = ((ICanCrud<OperationContract>)this).Get(key);
            if (!got.IsOk()) return new ActivityResult<IOperationService>(got, null);
            var contract = got.Payload;
            return _operationFactory.Create(contract);
        }


        public ActivityResult<IOperationService> Update(IOperationService toSave)
        {
            var v = Validate(toSave);
            if (!v.IsOk()) return new ActivityResult<IOperationService>(v, null);
            var savedContract = Update(toSave.Contract);
            if (!savedContract.IsOk()) return new ActivityResult<IOperationService>(savedContract);
            return _operationFactory.Create(savedContract.Payload);
        }

        public ActivityResult<IEnumerable<IOperationService>> AllForUser(string userId)
        {
            throw new NotImplementedException();
        }

        ActivityResult<OperationContract> ICanCrud<OperationContract>.Get(string key)
        {
            var got = _repo.Get<OperationContract>(key);
            if (!got.IsOk()) return new ActivityResult<OperationContract>(got, null);
            var contract = got.Payload;
            //TODO revisit this strategy
            switch (contract.OperationType)
            {
                case OperationType.Excel:
                    var excelMeta = _repo.Get<ExcelOperationMeta>(contract.Id);
                    if (excelMeta.IsOk()) contract.Meta = excelMeta.Payload;
                    return new ActivityResult<OperationContract>(new ExcelOperationContract(contract, excelMeta.Payload));
                case OperationType.Expression:
                    var exprMeta = _repo.Get<ExpressionOperationMeta>(contract.Id);
                    if (exprMeta.IsOk()) contract.Meta = exprMeta.Payload;
                    return new ActivityResult<OperationContract>(new ExpressionOperationContract(contract, exprMeta.Payload));
                default:
                    var gotMeta = _repo.Get<OperationMeta>(contract.Id);
                    if (gotMeta.IsOk()) contract.Meta = gotMeta.Payload;
                    return new ActivityResult<OperationContract>(gotMeta);
            }
        }

        public ActivityResult<OperationContract> Update(OperationContract toSave)
        {
            switch (toSave.OperationType)
            {
                case OperationType.InMemory:
                    var rtn = new ActivityResult<OperationContract>(null, StatusCode.Failed);
                    rtn.Message = "Cannot save the operation contract for an in-memory service to an external data store";
                    return rtn;

                case OperationType.Expression:
                    var expContract = toSave as ExpressionOperationContract;
                    if(expContract == null) return Utility.Failure<OperationContract>("The operation service provided declared itestlf as an expresssion service, but its contract could not be converted into an expression contract");
                    //TODO revisit this strategy
                    var baseCtr = _repo.Update<OperationContract>(expContract);
                    var expMeta = _repo.Update<ExpressionOperationMeta>((ExpressionOperationMeta)expContract.Meta);
                    return new ActivityResult<OperationContract>(new ExpressionOperationContract(baseCtr.Payload, expMeta.Payload));

                case OperationType.Input:
                    throw new NotImplementedException();

                case OperationType.HttpGet:
                    return _repo.Update<OperationContract>(toSave);

                case OperationType.Excel:
                    var excel = toSave as ExcelOperationContract;
                    if(excel == null) return Utility.Failure<OperationContract>("The operation service provided declared itestlf as an excel service, but its contract could not be converted into an excel contract");
                    //TODO revisit this strategy
                    var ctr = _repo.Update<OperationContract>(excel);
                    var meta = _repo.Update<ExcelOperationMeta>((ExcelOperationMeta)excel.MetaFor);
                    return new ActivityResult<OperationContract>(new ExcelOperationContract(ctr.Payload, meta.Payload));

                default:
                    rtn = new ActivityResult<OperationContract>(null, StatusCode.Failed);
                    rtn.Message = "Unsupported service type";
                    return rtn;
            }
        }

        public ActivityResult<OperationContract> Create(OperationContract toSave)
        {
            switch (toSave.OperationType)
            {
                case OperationType.InMemory:
                    var rtn = new ActivityResult<OperationContract>(null, StatusCode.Failed);
                    rtn.Message = "Cannot save the operation contract for an in-memory service to an external data store";
                    return rtn;

                case OperationType.Expression:
                    var expContract = toSave as ExpressionOperationContract;
                    if(expContract == null) return Utility.Failure<OperationContract>("The operation service provided declared itestlf as an expresssion service, but its contract could not be converted into an expression contract");
                    //TODO revisit this strategy
                    var baseCtr = _repo.Create<OperationContract>(expContract);
                    var expMeta = _repo.Create<ExpressionOperationMeta>((ExpressionOperationMeta)expContract.Meta);
                    return new ActivityResult<OperationContract>(new ExpressionOperationContract(baseCtr.Payload, expMeta.Payload));

                case OperationType.Input:
                    throw new NotImplementedException();

                case OperationType.HttpGet:
                    return _repo.Create<OperationContract>(toSave);

                case OperationType.Excel:
                    var excel = toSave as ExcelOperationContract;
                    if(excel == null) return Utility.Failure<OperationContract>("The operation service provided declared itestlf as an excel service, but its contract could not be converted into an excel contract");
                    //TODO revisit this strategy
                    var ctr = _repo.Create<OperationContract>(excel);
                    var meta = _repo.Create<ExcelOperationMeta>((ExcelOperationMeta)excel.MetaFor);
                    return new ActivityResult<OperationContract>(new ExcelOperationContract(ctr.Payload, meta.Payload));

                default:
                    rtn = new ActivityResult<OperationContract>(null, StatusCode.Failed);
                    rtn.Message = "Unsupported service type";
                    return rtn;
            }
        }

        public ActivityResult<IEnumerable<OperationContract>> Search(ISearchSpec<OperationContract> spec, SearchOption options = null)
        {
            throw new NotImplementedException();
        }
    }
}
