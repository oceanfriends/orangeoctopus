using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations.Contracts
{
    [DataContract]
    public class InputOnlyOperationContract : OperationContract
    {
        public InputOnlyOperationContract(string name, 
            TransputDefinition[] inputDefinitions,
            OperationType opType,
            OperationMeta meta)
        : base(name, inputDefinitions, TransputDefinition.EmptyArray, opType, meta) {}
    }


}
