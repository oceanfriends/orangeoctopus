﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations
{
    [DataContract]
    public class OperationMeta : Meta
    {
        public OperationMeta()
        { }
        [DataMember]
        public string Endpoint { get; set; }
        [DataMember]
        public Expression<Func<Transput[], string>> ResourceUriBuilder { get; set; }
        [DataMember]
        public string OperationId { get; set; }
        
    }
}
