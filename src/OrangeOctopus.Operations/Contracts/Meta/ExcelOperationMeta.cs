using System;
using OrangeOctopus.Shared;
using OrangeOctopus.Operations.Process;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace OrangeOctopus.Operations.Contracts
{
    [DataContract]
    public class ExcelOperationMeta : OperationMeta<ExcelOperation>
    {
        public ExcelOperationMeta(Dictionary<string, ExcelCell> mappings, AuthProperties authProps, string driveId, string workbookId)
        {
            AuthProperties = authProps;
            DriveId = driveId;
            WorkbookId = workbookId;
            ExcelParamMappings = mappings;
        }
        [DataMember]
        public AuthProperties AuthProperties { get; set; }
        [DataMember]
        public string DriveId { get; set; }
        [DataMember]
        public string WorkbookId { get; set; }
        [DataMember]
        public Dictionary<string, ExcelCell> ExcelParamMappings { get; private set; }
        
    }

    [DataContract]
    public struct ExcelCell
    {
        public ExcelCell(string worksheet, string column, uint row)
        {
            Row = row;
            Column = column;
            Worksheet = worksheet;
        }
        [DataMember]
        public string Worksheet { get; set; }
        [DataMember]
        public string Column { get; set; }
        [DataMember]
        public uint Row { get; set; }

        public string Address
        {
            get { return Worksheet + "!" + Column.ToString()  + Row; }
            set
            {
                // TODO: regex the value
                var worksheet = value.Split('!')[0];
                var cellAddress = value.Split('!')[1];

                var regex = new Regex(@"(?<col>([A-Z]|[a-z])+)(?<row>(\d)+)");
                var match = regex.Match(cellAddress);

                if (match != null)
                {
                    // TODO: check worksheet
                    this.Worksheet = worksheet;

                    Column = match.Groups["col"].Value;
                    Row = UInt32.Parse(match.Groups["row"].Value);
                }
                else
                {
                    throw new ArgumentException("Invalid input");
                }
            }
        }
    }
}
