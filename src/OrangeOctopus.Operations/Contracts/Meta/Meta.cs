﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations
{
    [DataContract]
    public class Meta
    {
        [DataMember]
        public string Description { get; set; }
    }
}
