using System;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using OrangeOctopus.Operations.Process;

namespace OrangeOctopus.Operations.Contracts
{
    [DataContract]
    public class ExpressionOperationMeta : OperationMeta<ExpressionOperation>
    {
        public ExpressionOperationMeta(Expression<Func<Transput[], Transput[]>> expressionRepresentation)
        {
            this.ExpressionRepresentation = expressionRepresentation;
        }
        [DataMember]
        public Expression<Func<Transput[], Transput[]>> ExpressionRepresentation { get; set; }
        
    }
}
