using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations.Contracts
{
    public class OperationContract<TOperation> : OperationContract where TOperation : IOperationService
    {
        public OperationContract(string name, 
            TransputDefinition[] inputDefinitions, 
            TransputDefinition[] outputDefinitions,
            OperationType opType,
            OperationMeta<TOperation> meta,
            string id = null)
         : base(name, inputDefinitions, outputDefinitions, opType, meta, id) 
        {
            MetaFor = meta;
        }

        public OperationContract(OperationContract seed, OperationMeta<TOperation> meta)
         : this(seed.Name, seed.InputDefinitions, seed.OutputDefinitions, seed.OperationType, meta, seed.Id) {}

        /// <summary>
        /// ctor for serializers and derivers
        /// </summary>
        protected OperationContract() {}

        public virtual OperationMeta<TOperation> MetaFor { get; set; }
    }


}
