using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations.Contracts
{
    [DataContract]
    public class OutputOnlyOperationContract : OperationContract
    {
        public OutputOnlyOperationContract(string name, 
            TransputDefinition[] outputDefinitions,
            OperationType opType,
            OperationMeta meta)
        : base(name, TransputDefinition.EmptyArray, outputDefinitions, opType, meta) {}
    }


}
