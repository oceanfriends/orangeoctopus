﻿using OrangeOctopus.Operations.Process;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace OrangeOctopus.Operations.Contracts
{
    [DataContract]
    public class ExcelOperationContract : OperationContract<ExcelOperation>
    {
        [JsonConstructor]
        public ExcelOperationContract(string name,
            TransputDefinition[] inputDefinitions,
            TransputDefinition[] outputDefinitions,
            ExcelOperationMeta meta,
            string id = null) : base(name, inputDefinitions, outputDefinitions, OperationType.Excel, meta, id)
        {
            //if we have any inputs or outputs defined (when would that not be the case?) validate that we have excel cells mapped
            if (inputDefinitions.Length != 0 || outputDefinitions.Length != 0)
            {
                if (meta.ExcelParamMappings == null) throw new ArgumentNullException(nameof(meta.ExcelParamMappings), "Excel param mappings were not defined");
                for (int i = 0; i < inputDefinitions.Length; i++)
                {
                    string paramName = inputDefinitions[i].Name;
                    //let it throw key not exists
                    if (!meta.ExcelParamMappings.ContainsKey(paramName)) throw new KeyNotFoundException($"The excel param mappings did not contain a definition for input {paramName}");
                }
                for (int i = 0; i < outputDefinitions.Length; i++)
                {
                    string paramName = outputDefinitions[i].Name;
                    //let it throw key not exists
                    if (!meta.ExcelParamMappings.ContainsKey(paramName)) throw new KeyNotFoundException($"The excel param mappings did not contain a definition for output {paramName}");
                }

            }
            MetaFor = meta;
        }

        public ExcelOperationContract(OperationContract seed, ExcelOperationMeta meta)
         : this(seed.Name, seed.InputDefinitions, seed.OutputDefinitions, meta, seed.Id) {}

    }
}
