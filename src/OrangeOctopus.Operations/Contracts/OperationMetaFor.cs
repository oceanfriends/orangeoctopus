using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations
{
    [DataContract]
    public class OperationMeta<TService> : OperationMeta where TService : IOperationService
    {
        
    }
}
