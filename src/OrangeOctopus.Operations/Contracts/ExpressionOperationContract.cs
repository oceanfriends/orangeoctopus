using OrangeOctopus.Operations.Process;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace OrangeOctopus.Operations.Contracts
{
    [DataContract]
    public class ExpressionOperationContract : OperationContract<ExpressionOperation>
    {
        [JsonConstructor]
        public ExpressionOperationContract(string name,
            TransputDefinition[] inputDefinitions,
            TransputDefinition[] outputDefinitions,
            ExpressionOperationMeta meta, string id = null) : base(name, inputDefinitions, outputDefinitions, OperationType.Expression, meta, id)
        {
            MetaFor = meta;
        }
        public ExpressionOperationContract(OperationContract seed, ExpressionOperationMeta meta)
         : this(seed.Name, seed.InputDefinitions, seed.OutputDefinitions,  meta, seed.Id) {}

    }
}
