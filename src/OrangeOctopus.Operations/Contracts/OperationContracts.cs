﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations
{
    [DataContract]
    public class OperationContract
    {
        public OperationContract(string name, 
            TransputDefinition[] inputDefinitions, 
            TransputDefinition[] outputDefinitions,
            OperationType opType,
            OperationMeta meta,
            string id = null)
        {
            if(inputDefinitions == null || inputDefinitions.Length == 0) throw new ArgumentOutOfRangeException(nameof(inputDefinitions));
            if(outputDefinitions == null || outputDefinitions.Length == 0) throw new ArgumentOutOfRangeException(nameof(inputDefinitions));

            Id = (String.IsNullOrWhiteSpace(id)) ? Guid.NewGuid().ToString() : id;
            Name = name;
            InputDefinitions = inputDefinitions;
            OutputDefinitions = outputDefinitions;
            OperationType = opType;
            Meta = meta;
            Meta.OperationId = Id;
        }

        /// <summary>
        /// ctor for derivers and serializers
        /// </summary>
        protected OperationContract() {}
        [DataMember]
        public string Name { get; internal set; }
        [DataMember]
        public virtual OperationMeta Meta { get; set; }
        [DataMember]
        public virtual TransputDefinition[] InputDefinitions { get; internal set; }
        [DataMember]
        public virtual TransputDefinition[] OutputDefinitions { get; internal set; }
        [DataMember]
        public OperationType OperationType { get; set; }
        [DataMember]
        public string Id { get; set; }
    }


}
