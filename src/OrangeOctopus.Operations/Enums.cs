﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations
{
    public enum OperationType
    {
        InMemory = 1,
        Expression = 2,
        Input = 3,
        HttpGet = 4,
        HttpPost = 5,
        Excel = 6,
        ConsoleInput = 7,
        ConsoleOutput = 8,
    }

}
