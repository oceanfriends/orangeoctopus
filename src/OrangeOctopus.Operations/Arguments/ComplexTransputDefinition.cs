﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations
{
    public class ComplexTransputDefinition : TransputDefinition
    {
        public IEnumerable<TransputDefinition> Children { get; set; }
        public override bool? Equivalent(TransputDefinition other)
        {
            var baseRes = base.Equivalent(other);
            if (!baseRes.HasValue)
            {
                //fuck this is where order comes into play... unless we just match up on type code? but what if they are 
                //nested structures as well? We basically would need to brute force every combination of potentially viable
                //types... what a fucking mess
                return true;
            }
            return baseRes;
        }

    }
}
