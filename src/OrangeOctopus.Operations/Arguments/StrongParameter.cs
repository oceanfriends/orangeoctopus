﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations
{
    public class StrongParameter<T> : Parameter
    {
        public StrongParameter(T val)
        {
            Value = val;
        }
        protected override object InternalVal { get => Value; set => Value = (T)value; }
        public new T Value { get; set; }
    }
}
