﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations
{
    public abstract class Parameter //: IParameter
    {
        public object Value { get => this.InternalVal; set => this.InternalVal = value; }
        protected abstract object InternalVal { get; set; }
    }
}
