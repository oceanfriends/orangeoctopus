﻿using Benj.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations
{
    [DataContract]
    [Serializable]
    public class StructureType : TypedEnum, ISerializable
    {
        public static StructureType Empty { get; } = new EmptyArgument();
        public static StructureType Single { get; } = new SingleArgument();
        public static StructureType Range { get; } = new RangeArgument();
        public static StructureType Complex { get; } = new ComplexAgument();
        public static StructureType ComplexRange { get; } = new ComplexRangeAgument();

        private StructureType(string name, int value) : base(name, value)
        { }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        // Constructor should be protected for unsealed classes, private for sealed classes.
        // (The Serializer invokes this constructor through reflection, so it can be private)
        private StructureType(SerializationInfo info, StreamingContext context) : base(null, default(int))
        {
            this.Name = info.GetString("Name");
            this.Value = info.GetInt32("Value");
        }

        public static IEnumerable<StructureType> List()
        {
            // alternately, use a dictionary keyed by value
            return new[]{Single,Range,Complex,ComplexRange};
        }
     
        public static StructureType FromString(string status)
        {
            return List().Single(r => String.Equals(r.Name, status, StringComparison.OrdinalIgnoreCase));
        }
     
        public static StructureType FromValue(int value)
        {
            return List().Single(r => r.Value == value);
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", this.Name);
            info.AddValue("Value", this.Value);
        }

        public override bool Equals(object obj)
        {
            if(base.Equals(obj) == true) return true;
            if (obj is StructureType arg)
            {
                return arg.Name == this.Name && arg.Value == this.Value;
            }
            return false;
        }
        public bool Equals(StructureType arg) => arg.Name == this.Name && arg.Value == this.Value;
        public override int GetHashCode() => this.Name.GetHashCode() + this.Value.GetHashCode();

        #region private enum definitions

        private class EmptyArgument : StructureType
        {
            internal EmptyArgument() : base("Empty", 0) { }
        }
        private class SingleArgument : StructureType
        {
            internal SingleArgument() : base("Single", 1) { }
        }
        private class RangeArgument : StructureType
        {
            internal RangeArgument() : base("Range", 2) { }
        }
        private class ComplexAgument : StructureType
        {
            internal ComplexAgument() : base("Complex", 3) { }
        }
        private class ComplexRangeAgument : StructureType
        {
            internal ComplexRangeAgument() : base("ComplexRange", 4) { }
        }

        #endregion
    }
}
