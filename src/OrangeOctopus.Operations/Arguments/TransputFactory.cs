﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations
{
    public class TransputFactory
    {
        public static Transput<T> CreateFrom<T>(T val, string name)
        {
            var defn = DefinitionFromType<T>(name);
            return new Transput<T>(val, defn);
        }
        public static Transput<T> CreateFrom<T>(T val, TransputDefinition defn)
        {
            var def = DefinitionFromType<T>("bliggity bloo bla");
            if (!defn.Equivalent(def).GetValueOrDefault())
                throw new ArgumentException("The type you passed in does not match the function argument definition you provided");
            return new Transput<T>(val, defn);
        }
        public static Transput Create(object val, TransputDefinition defn)
        {
            var inputyTypeCode = Type.GetTypeCode(val.GetType());
            try
            {
                var typedVal = Convert.ChangeType(val, defn.TypeCode);
            }
            catch (Exception ex)
            {
                throw new ArgumentException("The type you passed in does not match the function argument definition you provided", ex);
            }
            return new Transput(val, defn);
        }

        public static TransputDefinition DefinitionFromType<T>(string name, bool required=true)
        {
            var t = typeof(T);
            var argType = Utility.DetermineStructureType<T>();
            var typeCode = Type.GetTypeCode(t);
            if (typeCode == TypeCode.Object)
            {
                var defn = new ComplexTransputDefinition()
                {
                    StructureType = argType,
                    TypeName = t.FullName,
                    TypeCode = Type.GetTypeCode(t),
                    Name = name,
                    Required = required
                };
                return defn;

            }
            else
            {
                var defn = new TransputDefinition()
                {
                    StructureType = argType,
                    TypeName = t.FullName,
                    TypeCode = Type.GetTypeCode(t),
                    Name = name,
                    Required = required
                };
                return defn;
            }
        }
    }
}
