﻿using Benj.Common;
using Benj.Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations
{
    public class Transput : Parameter
    {
        public string Name => Definition.Name;
        public Transput(object val, TransputDefinition defn)
        {
            Value = val;
            Definition = defn;
        }
        public TransputDefinition Definition { get; set; }
        protected override object InternalVal { get; set; }

        public static Transput<Empty> Empty = new Transput<Empty>(new Empty(), TransputDefinition.Empty);
        public static Transput None = new Transput(new Empty(), TransputDefinition.Empty);
        public static Transput[] NoneArray = new[] { None };
    }
    public class Transput<T> : StrongParameter<T>
    {
        public TransputDefinition ArgumentDefinition { get; set; }

        public Transput(T val, TransputDefinition defn) : base(val)
        {
            this.Value = val;
            this.ArgumentDefinition = defn;
        }

        public static implicit operator Transput(Transput<T> functionParameter)
        {
            return new Transput(functionParameter.Value, functionParameter.ArgumentDefinition);
        }

        public static ActivityResult<Transput<T>> FromTransput(Transput param)
        {
            return FromTransput(param, param.Definition);

        }
        public static ActivityResult<Transput<T>> FromTransput(Transput param, TransputDefinition defn)
        {
            if (param.Value is T val)
            {

                try
                {
                    var strongParam = TransputFactory.CreateFrom(val, defn);
                    var ar = new ActivityResult<Transput<T>>(strongParam, StatusCode.Ok);
                    return ar;
                }
                catch (Exception ex)
                {
                    return new ActivityResult<Transput<T>>(ex);
                }
            }
            else
            {
                var rtn = new ActivityResult<Transput<T>>(null, StatusCode.Failed);
                rtn.Message = $"Could not convert from the input param {param.Definition.Name} to type {typeof(T).Name}";
                return rtn;
            }

        }
    }
}
