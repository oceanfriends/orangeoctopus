﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations
{
    [DataContract]
    [Serializable]
    public class TransputDefinition 
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public bool Required { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public StructureType StructureType { get; set; }
        [DataMember]
        public TypeCode TypeCode { get; set; }
        [DataMember]
        public string TypeName { get; set; }
        public static TransputDefinition Empty = 
             new TransputDefinition()
            {
                StructureType = StructureType.Empty,
                TypeName = "Empty",
                Name = "EmptyArgument",
                Required = false,
                TypeCode = TypeCode.Empty,
            };

        public static TransputDefinition[] EmptyArray = new[] { Empty };

        [OnDeserialized()]
        internal void OnDeserialized(StreamingContext context)
        {
            if (this.StructureType.Equals(StructureType.Empty))
            {
                this.StructureType = StructureType.Empty;
            }
            else if (this.StructureType.Equals(StructureType.Single))
            {
                this.StructureType = StructureType.Single;
            }
            else if (this.StructureType.Equals(StructureType.Range))
            {
                this.StructureType = StructureType.Range;
            }
            else if (this.StructureType.Equals(StructureType.Complex))
            {
                this.StructureType = StructureType.Complex;
            }
            else if (this.StructureType.Equals(StructureType.ComplexRange))
            {
                this.StructureType = StructureType.ComplexRange;
            }
        }

        public virtual bool? Equivalent(TransputDefinition other)
        {
            if (this.StructureType.Equals(other.StructureType) && this.TypeCode == other.TypeCode)
            {
                if (this.TypeCode != TypeCode.Object)
                    return true;
                else
                {
                    //impossible to tell if we are dealing with complex types. 
                    //Derived classes dealing with complex types should override this method
                    return null; 
                }
            }
            return false;
        }
    }
}
