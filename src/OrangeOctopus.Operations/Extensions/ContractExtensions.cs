using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations
{
    public static class ContractExtensions
    {
        public static bool IsSingleOutput (this OperationContract src)
        {
            if(src.OutputDefinitions == null) throw new ArgumentNullException(nameof(src.OutputDefinitions));
            if(src.OutputDefinitions.Length == 0) throw new ArgumentOutOfRangeException(nameof(src.OutputDefinitions), "The provided contract has no output definitions");
            return src.OutputDefinitions[0].StructureType.Equals(StructureType.Single);
        }
        public static bool IsSingleOutput<T>(this OperationContract src)
        {
            if(!src.IsSingleOutput()) return false;
            return src.OutputDefinitions[0].Equivalent(TransputFactory.DefinitionFromType<T>("bloop")).GetValueOrDefault();
        }
        
    }
}
