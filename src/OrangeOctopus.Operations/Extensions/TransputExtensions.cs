﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations
{
    public static class TransputExtensions
    {
        public static TransputDefinition[] ToArray(this TransputDefinition src)
        {
            return new[] { src };
        }
        public static Transput[] ToArray(this Transput src)
        {
            return new[] { src };
        }
        public static Transput<T>[] ToStrongArray<T>(this Transput<T> src)
        {
            return new[] { src };
        }
        public static Transput[] ToArray<T>(this Transput<T> src)
        {
            return new Transput[] { src };
        }
        public static Transput[] ToArray<T>(this Transput<T>[] src)
        {
            Transput[] rtn = new Transput[src.Length];
            for(var i = 0; i < src.Length; i++)
            {
                rtn[i] = (Transput)src[i];
            }
            return rtn;
        }
        
    }
}
