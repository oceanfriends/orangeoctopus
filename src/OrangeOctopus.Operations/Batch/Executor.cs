﻿using OrangeOctopus.Operations.Contracts;
using OrangeOctopus.Operations.Process;
using OrangeOctopus.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Benj.Common;
using Benj.Common.Entities;

namespace OrangeOctopus.Operations.Batch
{
    public class Executor
    {
        private IOperationFactory _operationFactory;

        public Executor(IOperationFactory operationFactory)
        {
            this._operationFactory = operationFactory;
        }

        public async Task Execute(BatchProcess process)
        {
            Transput[] previousOutputs = null;
            for (int i = 0; i < process.Steps.Count; i++)
            {
                var step = process.Steps[i];
                var operationRes = _operationFactory.Create(step.OperationDef);
                if(!operationRes.IsOk()) throw new InvalidOperationException(operationRes.Message);
                var operation = operationRes.Payload;
                
                var inputs = (step.PropertyMaps != null && previousOutputs != null) ? MapInputs(previousOutputs, step.OperationDef, step.PropertyMaps) : null;
                previousOutputs = await operation.ExecuteAsync(inputs?.ToArray());
            }
        } 

        public List<Transput> MapInputs(Transput[] previousOutputs, OperationContract definition, List<Map> maps)
        {
            var excelDefinition = definition as ExcelOperationContract;
            var genericDefinition = definition as OperationContract;

            List<TransputDefinition> inputDefs = null;

            if(excelDefinition != null)
            {
                inputDefs = excelDefinition.InputDefinitions.Cast<TransputDefinition>().ToList();
            }
            else if(genericDefinition != null)
            {
                inputDefs = genericDefinition.InputDefinitions.ToList();
            }
            else
            {
                throw new InvalidOperationException("Operation type parameters not supported.");
            }

            var inputs = from def in inputDefs
                         join map in maps on def.Name equals map.To
                         join prev in previousOutputs on map.From equals prev.Name
                         select new Transput(
                            prev.Value,
                            new TransputDefinition
                            {
                                Name = def.Name,
                                TypeCode = prev.Definition.TypeCode,
                                StructureType = prev.Definition.StructureType,
                                Required = prev.Definition.Required
                            }
                         );

            return inputs.ToList();
        }
    }
}
