﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrangeOctopus.Operations.Batch
{
    public class BatchStep
    {
        public OperationContract OperationDef { get; set; }
        public List<Map> PropertyMaps { get; set; }
    }
}
