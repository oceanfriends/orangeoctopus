﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrangeOctopus.Operations.Batch
{
    public class Map
    {
        public string From { get; set; }
        public string To { get; set; }
    }
}
