﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrangeOctopus.Operations.Batch
{
    public class BatchProcess
    {
        public string Name { get; set; }
        public List<BatchStep> Steps { get; set; }
    }
}
