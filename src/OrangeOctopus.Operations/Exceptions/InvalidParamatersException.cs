﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations
{
    [Serializable]
    public class InvalidParamatersException : Exception
    {
        public InvalidParamatersException() : base()
        { }
        public InvalidParamatersException(string message) : base(message)
        { }
        public InvalidParamatersException(string message, Exception innerException) : base(message, innerException)
        { }
        public InvalidParamatersException(SerializationInfo info, StreamingContext context) : base(info, context)
        { }
        
    }
}
