﻿using Benj.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations
{
    public class Validation
    {
        /// <summary>
        /// Iterates through an array of function definitions and paired input paramaters to determine if the input provided is valid based simply
        /// on argument counts, argument names and basic types
        /// </summary>
        /// <param name="inputs"></param>
        /// <param name="defs"></param>
        /// <returns></returns>
        public static ActivityResult BasicNamedValidator(Transput[] inputs, TransputDefinition[] defs)
        {
            if (defs == null) throw new ArgumentNullException(nameof(defs), "Vaidation failed because no input definitions were provided");
            inputs = (inputs == null) ? Transput.NoneArray : inputs;
            if (inputs.Any(x => x.Definition == null)) throw new ArgumentNullException(nameof(TransputDefinition));

            var rtn = new ActivityResult(StatusCode.Failed);

            //handle the case for no-required-input definitions (eg Action<T>)
            if (defs.Length == 0 || (defs.Length == 1 && defs[0] == TransputDefinition.Empty))
            {
                if (inputs.Length == 0)
                {
                    rtn.Status = StatusCode.Ok;
                    return rtn;
                }
                else if (inputs.Length > 1)
                {
                    rtn.Status = StatusCode.Failed;
                    rtn.Message = "More than the allowed parameters were passed in for a argument definition accepting empty inputs";
                    return rtn;
                }
                else
                {
                    rtn.Status = (inputs[0].Definition == TransputDefinition.Empty) ? StatusCode.Ok : StatusCode.Failed;
                    return rtn;
                }
            }

            int requiredCount = defs.Count(x => x.Required);
            if (inputs.Length < requiredCount)
            {
                rtn.Status = StatusCode.Failed;
                rtn.Message = "Not enough arguments were passed in";
                return rtn;
            }

            foreach (var def in defs)
            {
                var currentInput = inputs.Where(x => x.Definition.Name == def.Name);
                if (currentInput.Count() > 1)
                {
                    rtn.Status = StatusCode.Failed;
                    rtn.Message = $"There were multiple matching inputs for the required parameter {def.Name}";
                }

                if (def.Required && currentInput.Count() == 0)
                {
                    rtn.Status = StatusCode.Failed;
                    rtn.Message = $"No input was provided for the required input {def.Name}";
                    return rtn;
                }

                if (!def.Required && currentInput.Count() == 0) continue;

                var current = currentInput.First();
                //handle complex types
                if (current.Definition.TypeCode == TypeCode.Object)
                {
                    var currentComplex = current.Definition as ComplexTransputDefinition;
                    var defComplex = def as ComplexTransputDefinition;
                    if (currentComplex == null || defComplex == null)
                    {
                        rtn.Status = StatusCode.Failed;
                        rtn.Message = "A complex parameter was passed in, but either it was not constructed" +
                            $" as a complex definition or the corresponding parameter is not a complex argument. Parameter: {def.Name}";
                        return rtn;
                    }
                    bool samesies = defComplex.Equivalent(currentComplex).GetValueOrDefault();
                    if (!samesies)
                    {
                        rtn.Status = StatusCode.Failed;
                        rtn.Message = $"The input for parameter {def.Name} was not of a compatible structure";
                        return rtn;
                    }

                }
                else
                {
                    bool samesies = def.Equivalent(current.Definition).GetValueOrDefault();
                    if (!samesies)
                    {
                        rtn.Status = StatusCode.Failed;
                        rtn.Message = $"The input for parameter {def.Name} was not of a compatible structure";
                        return rtn;
                    }
                }

            }

            rtn.Status = StatusCode.Ok;
            return rtn;
        }
        
    }
}
