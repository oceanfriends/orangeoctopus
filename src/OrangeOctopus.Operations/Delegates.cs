﻿using Benj.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Operations
{
    public delegate Transput[] InMemExecutor(params Transput[] inputs);
    public delegate ActivityResult InMemValidator(params Transput[] inputs);
}
