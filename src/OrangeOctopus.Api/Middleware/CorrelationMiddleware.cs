﻿    using System;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.AspNetCore.Http;

namespace OrangeOctopus.Api.Middleware
{
    public class CorrelationMiddleware
    {
        public CorrelationMiddleware(RequestDelegate next, ILogger logger)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _logger = logger;
        }
		public const string CorrelationIdHeader = "x-correlationid";
		private readonly RequestDelegate _next;
		private readonly ILogger _logger;

		public async Task Invoke(HttpContext context)
		{
            var correlationAccessor = context.RequestServices.GetService<ICorrelationProvider>();

			try
			{
                if (correlationAccessor != null)
                {
                    //if we have a correlation id, let's use that as the correlation id and give ourselves a new operation id
                    if (context.Request.Headers.Keys.Contains(CorrelationIdHeader))
                    {
                        string correlationString = context.Request.Headers[CorrelationIdHeader].ToString();
                        if (Guid.TryParse(correlationString, out Guid corrGuid))
                            correlationAccessor.RequestCorrelationId = corrGuid;
                    }
                    //otherwise, both the correlation id and the operation id are the same
                    else
                    {
                        var guid = Guid.NewGuid();
                        correlationAccessor.RequestCorrelationId = guid;
                    }
                }
                context.Response.Headers[CorrelationIdHeader] = correlationAccessor?.RequestCorrelationId.ToString() ?? Guid.Empty.ToString(); //something went wrong, at least this will show us in the logs
			}
			catch (Exception ex)
			{
				_logger.LogError(0, ex, "Failed to create correlationAccessor for request. Headers: '{Headers}'", context.Request.Headers.ToString());
			}

			await _next.Invoke(context);

		}
        
    }
}