﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OrangeOctopus.Api.Middleware
{
    public interface ICorrelationProvider
    {
        Guid RequestCorrelationId { get; set; }
        Guid GetCorrelationId(Guid operationId);
    }
    public class CorrelationProvider : ICorrelationProvider
    {
        private AsyncLocal<Guid> _correlationId = new AsyncLocal<Guid>();
        public Guid RequestCorrelationId { get => _correlationId.Value; set => _correlationId.Value = value; }

        public Guid GetCorrelationId(Guid operationId)
        {
            throw new NotImplementedException();
        }
    }
}
