using System;
using OrangeOctopus.Operations;
using OrangeOctopus.Operations.Persistance;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Benj.Common;
using OrangeOctopus.Data.Search;
using OrangeOctopus.Api.Middleware;
using Microsoft.Extensions.Logging;
using OrangeOctopus.Api.Contracts;
using Benj.Common.Data;
using OrangeOctopus.Api.Mappings;

namespace OrangeOctopus.Api.Controllers
{
    /// <summary>
    /// Should we even have a single controller, or should we have a controller for each operation type?
    /// </summary>
    [Route("api/[controller]")]
    public class OperationsController : OrangeController
    {
        private readonly IOperationRegistry _registry;
        private ICanCrud<OperationContract> _repo;

        public OperationsController(IOperationRegistry repo, IOrangeMapper mapper,
            ICorrelationProvider corr, ILogger logger) : base(mapper, corr, logger)
        {
            _registry = repo;
            _repo = (ICanCrud<OperationContract>)_registry;
        }

        [HttpGet]
        public ActivityResult<IEnumerable<OperationDto>> Get([FromQuery] OperationContractSearch search)
        {
            search.AddBaseSearchContext(this);
            ActivityResult<IEnumerable<OperationContract>> all = _registry.Search(GetDefaultSearchSpec<OperationContractSearch>());
            return FromResult<OperationContract, OperationDto>(all);
        }

        [HttpGet("{id}")]
        public ActivityResult<OperationDto> Get(string id)
        {
            var lookup = _repo.Get(id);
            return FromResult<OperationContract, OperationDto>(lookup);
        }

        [HttpPost]
        public ActivityResult<OperationDto> Post([FromBody]OperationDto value)
        {
            var toSave = _mapper.Map<OperationDto, OperationContract>(value);
            //TODO hydrate it up
            var res = _registry.Create(toSave);
            return FromResult<OperationContract, OperationDto>(res);
        }

        [HttpPut("{id}")]
        public void Put(string id, [FromBody]string value)
        {
            throw new NotImplementedException();
        }

        [HttpDelete("{id}")]
        public ActivityResult Delete(string id)
        {
            return _repo.Delete(id);
        }
    }
}
