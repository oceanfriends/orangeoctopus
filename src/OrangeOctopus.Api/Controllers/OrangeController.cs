using System;
using OrangeOctopus.Operations;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Benj.Common;
using Benj.Common.Entities;
using OrangeOctopus.Shared;
using OrangeOctopus.Data.Search;
using Microsoft.Extensions.Logging;
using System.Runtime.ExceptionServices;
using OrangeOctopus.Api.Middleware;
using OrangeOctopus.Api.Mappings;

namespace OrangeOctopus.Api.Controllers
{
    public abstract class OrangeController : Controller
    {
        protected readonly IOrangeMapper _mapper;
        protected readonly ILogger _logger;
        protected readonly ICorrelationProvider _corr;

        public string CurrentUser => User.FindFirst(ClaimTypes.NameIdentifier).Value;

        public OrangeController(IOrangeMapper mapper, ICorrelationProvider corr, ILogger logger)
        {
            _mapper = mapper;
            _logger = logger;
            _corr = corr;
        }

        protected ActivityResult<TContract> FromResult<TDomain, TContract>(ActivityResult<TDomain> result)
        {
            if (result.IsOk()) return new ActivityResult<TContract>(result, _mapper.Map<TDomain, TContract>(result.Payload));
            else return Helper.FailedFromSeed<TContract>(result, result.Message ?? "Failed with nondescript error");
        }
        protected ActivityResult<IEnumerable<TContract>> FromResult<TDomain, TContract>(ActivityResult<IEnumerable<TDomain>> result)
        {
            if (result.IsOk())
            {
                var rtn =_mapper.Map<IEnumerable<TDomain> ,IEnumerable<TContract>>(result.Payload);
                return new ActivityResult<IEnumerable<TContract>>(result, rtn);
            }
            else return Helper.FailedFromSeed<IEnumerable<TContract>>(result, result.Message ?? "Failed with nondescript error");
        }
        protected ActivityResult ReturnFailedOrThrow(ActivityResult res, bool badRequest = false)
        {
            if (badRequest) HttpContext.Response.StatusCode = 400;
            if (res.IsOk()) throw new InvalidOperationException("A succesful operation was treated as unsuccesful");
            _logger.Log(res.ToLogLevel(), 0, res, res.Exception, (msg, ex) => $"{res.Message} | corrId: {res.CorrelationId} | " +
            $"opId: {res.OperationId} | ex: {res.Exception}");
            if (res.Exception != null) ExceptionDispatchInfo.Capture(res.Exception).Throw();
            return res;
        }
        protected ActivityResult<T> ReturnFailedOrThrow<T>(ActivityResult res, bool badRequest = false)
        {
            if (badRequest) HttpContext.Response.StatusCode = 400;
            if (res.IsOk()) throw new InvalidOperationException("A succesful operation was treated as unsuccesful");
            _logger.Log(res.ToLogLevel(), 0, res, res.Exception, (msg, ex) => $"{res.Message} | corrId: {res.CorrelationId} | " +
            $"opId: {res.OperationId} | ex: {res.Exception}");
            if (res.Exception != null) ExceptionDispatchInfo.Capture(res.Exception).Throw();
            return new ActivityResult<T>(res);
        }

        protected TSpec GetDefaultSearchSpec<TSpec>() where TSpec : BaseSearchContract, new()
        {
            var spec = new TSpec()
            {
                UserId = CurrentUser,
                OrganizationId = "" //TODO
            };
            return spec;
        }
    }
}
