﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Benj.Common.Data;
using Benj.Common;
using OrangeOctopus.Api.Contracts;
using OrangeOctopus.Operations.Persistance;
using Benj.Common.Entities;
using OrangeOctopus.Operations;
using Microsoft.Extensions.Logging;
using OrangeOctopus.Api.Middleware;
using OrangeOctopus.Api.Mappings;

namespace OrangeOctopus.Api.Controllers
{
    [Route("api/[controller]")]
    public class CalculationsController : OrangeController
    {
        private readonly IAmARepo _repo;
        private readonly IOperationRegistry _registry;

        public CalculationsController(IAmARepo repo, IOperationRegistry registry, 
            IOrangeMapper mapper, ICorrelationProvider corr, ILogger logger) : base(mapper, corr, logger)
        {
            _repo = repo;
            _registry = registry;
        }

        [HttpPost]
        public async Task<ActivityResult<CalculationResponseDto>> Post([FromBody]CalculationRequestDto request)
        {
            var calcRes = ((ICanCrud<IOperationService>)_registry).Get(request.OperationId);
            if (!calcRes.IsOk()) ReturnFailedOrThrow(calcRes);
            IOperationService svc = calcRes.Payload;
            Transput[] inputs = new Transput[svc.Contract.InputDefinitions.Length];
            for (int i = 0; i < svc.Contract.InputDefinitions.Length; i++)
            {
                TransputDefinition def = svc.Contract.InputDefinitions[i];
                try
                {
                    object val = request.Inputs[def.Name];
                    inputs[i] = TransputFactory.Create(val, def);
                }
                catch (Exception ex)
                {
                    HttpContext.Response.StatusCode = 400;
                    _logger.LogWarning(ex, $"Request could not be matched to parameter {def.Name}");
                    throw;
                }
            }

            var valid = svc.ValidateArguments(inputs);
            if (!valid.IsOk()) return ReturnFailedOrThrow<CalculationResponseDto>(valid);
            var res = await svc.ExecuteAsync(inputs);
            var outputs = res.ToDictionary(t => t.Name, t => t.Value);
            var response = new CalculationResponseDto()
            {
                CorrelationId = _corr.RequestCorrelationId,
                OperationId = request.OperationId,
                Outputs = outputs
            };
            return new ActivityResult<CalculationResponseDto>(response);
        }

        [HttpPut("{id}")]
        public void Put(string id, [FromBody]string value)
        {
            throw new NotImplementedException();
        }

        [HttpDelete("{id}")]
        public ActivityResult Delete(string id)
        {
            return _repo.Delete<OperationContract>(id);
        }
    }
}
