﻿using Benj.Common;
using Microsoft.Extensions.Logging;
using OrangeOctopus.Data.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Api.Controllers
{
    public static class Extensions
    {
        public static void AddBaseSearchContext(this BaseSearchContract src, OrangeController ctr) 
        {
            src.UserId = ctr.CurrentUser;
            src.OrganizationId = "";
        }

        public static LogLevel ToLogLevel(this ActivityResult result)
        {
            if (result.Status == StatusCode.Ok) return LogLevel.Information;
            if (result.Status == StatusCode.Failed) return LogLevel.Warning;
            if (result.Status == StatusCode.Exception) return LogLevel.Error;
            return LogLevel.Information;
        }
        
    }
}
