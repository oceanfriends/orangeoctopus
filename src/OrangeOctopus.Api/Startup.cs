﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging.Debug;
using OrangeOctopus.Api.Middleware;
using Benj.Common.Data;
using OrangeOctopus.Data.Work;
using OrangeOctopus.Operations;
using OrangeOctopus.Operations.Persistance;
using OrangeOctopus.Operations.Process;
using OrangeOctopus.Operations.Contracts;
using OrangeOctopus.Shared;
using AutoMapper;
using OrangeOctopus.Api.Mapping;
using Newtonsoft.Json.Converters;
using OrangeOctopus.Api.Mappings;
using OrangeOctopus.Api.Contracts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Text;

namespace OrangeOctopus.Api
{
    public class Startup
    {
        public bool ACTIVATE_AUTH = false;
        private static Guid _tempExcelGuid = new Guid("ff1dd937-22a7-424d-bdc0-550b525b0726");
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            if (ACTIVATE_AUTH)
            {
                services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(o =>
                {
                    o.Authority = string.Format("https://login.microsoftonline.com/tfp/{0}/{1}/v2.0/",
                        Configuration["Authentication:AzureAd:Tenant"], Configuration["Authentication:AzureAd:Policy"]);
                    o.Audience = Configuration["Authentication:AzureAd:ClientId"];
                    o.Events = new JwtBearerEvents
                    {
                        OnAuthenticationFailed = AuthenticationFailed
                    };
                });
            }


            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.Converters.Add(new StringEnumConverter());
            });

            services.AddTransient<ILogger>(ctx => new DebugLogger("Temp"));
            services.AddSingleton<ICorrelationProvider, CorrelationProvider>();
            (IAmARepo repo, IOperationRegistry reg) = TempRepo();
            services.AddSingleton<IAmARepo>(ctx => repo);
            services.AddSingleton<ICanCrud>(ctx => repo);
            services.AddSingleton<IOperationFactory, OperationFactory>();
            services.AddSingleton<IOperationRegistry>(reg);
            services.AddSingleton<IMapper>(ctx =>
            {
                var config = new MapperConfiguration(cfg => cfg.AddProfile<OperationContractProfile>());
                return config.CreateMapper();
            });
            services.AddSingleton<IOrangeMapper>(ctx =>
            {
                var mappa = new OrangeMapper(ctx.GetRequiredService<IMapper>());
                mappa.AddEnricher<OperationDto, OperationContract>(Enrichers.EnrichOperationFromDto);
                return mappa;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }


            ScopeRead = Configuration["Authentication:AzureAd:ScopeRead"];
            ScopeWrite = Configuration["Authentication:AzureAd:ScopeWrite"];

            app.UseExceptionHandler("/Shared/Error");
            app.UseStaticFiles();
            app.UseMiddleware<CorrelationMiddleware>();
            app.UseMvcWithDefaultRoute();
        }

        private (IAmARepo repo, IOperationRegistry registry) TempRepo()
        {
            var repo = new TempRepo();
            repo.AddKeyStrategy<OperationContract>(x => x.Id);
            repo.AddKeyStrategy<ExcelOperationContract>(x => x.Id);
            repo.AddKeyStrategy<ExpressionOperationContract>(x => x.Id);
            repo.AddKeyStrategy<ExpressionOperationMeta>(x => x.OperationId);
            repo.AddKeyStrategy<ExcelOperationMeta>(x => x.OperationId);
            repo.AddKeyStrategy<OperationMeta>(x => x.OperationId);

            repo.AddKeyStrategy<IOperationService>(x => x.Contract.Id);

            var auth = new AuthProperties
            {
                Authority = "https://login.microsoftonline.com/" + "750cfc55-ab3a-4785-986a-aca285210979", // base url + tenant Id
                ApplicationId = "315039c5-45e3-4f9f-a197-a8bf2d80c04a", // calculation-service
                ApplicationSecret = "xcR8num62NbBqjqshjNdVer",
            };

            var mappings = new Dictionary<string, ExcelCell>()
            {
                ["ExcelInput1"] = new ExcelCell("Calculation", "A", 1),
                ["ExcelInput2"] = new ExcelCell("Calculation", "A", 2),
                ["ExcelOutputMultiplication"] = new ExcelCell("Calculation", "A", 3),
                ["ExcelOutputSum"] = new ExcelCell("Calculation", "A", 4),
            };

            var excelMeta = new ExcelOperationMeta(mappings, authProps: auth,
                    driveId: "b!UDtp5_XzbUGVF6Y4Pun6OK3B4nati_lAlio4QYgNvAexUC1OoHD1T5xQ3Y9YnwJL",
                    workbookId: "01DXZCDMVEWPIDEDQOI5HYNQXKLZUHZ7IN");

            var ctr = new ExcelOperationContract
            (name: "Test Operation 1", meta: excelMeta,
                inputDefinitions: new TransputDefinition[] {
                    new TransputDefinition
                    {
                        Name = "ExcelInput1",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    },
                    new TransputDefinition
                    {
                        Name = "ExcelInput2",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    }
                },
                outputDefinitions: new TransputDefinition[] {
                    new TransputDefinition
                    {
                        Name = "ExcelOutputMultiplication",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    },
                    new TransputDefinition
                    {
                        Name = "ExcelOutputSum",
                        StructureType = StructureType.Single,
                        TypeCode = TypeCode.Double,
                        Required = true,
                    }
                }, id: _tempExcelGuid.ToString());
            var registry = new OperationRegistry(repo, new OperationFactory());
            registry.Create(new ExcelOperation(ctr, new ExcelAuthService()));
            return (repo, registry);
        }

        private static AuthProperties tempExcelShit = new AuthProperties
            {
                Authority = "https://login.microsoftonline.com/" + "750cfc55-ab3a-4785-986a-aca285210979", // base url + tenant Id
                ApplicationId = "315039c5-45e3-4f9f-a197-a8bf2d80c04a", // calculation-service
                ApplicationSecret = "xcR8num62NbBqjqshjNdVer",
            };

        private Task AuthenticationFailed(AuthenticationFailedContext arg)
        {
            // For debugging purposes only!
            var s = $"AuthenticationFailed: {arg.Exception.Message}";
            arg.Response.ContentLength = s.Length;
            arg.Response.Body.Write(Encoding.UTF8.GetBytes(s), 0, s.Length);
            return Task.FromResult(0);
        }

        private static string tempExcelDriveIdBS = "b!UDtp5_XzbUGVF6Y4Pun6OK3B4nati_lAlio4QYgNvAexUC1OoHD1T5xQ3Y9YnwJL";
        public static string ScopeRead;
        public static string ScopeWrite;
    }
}
