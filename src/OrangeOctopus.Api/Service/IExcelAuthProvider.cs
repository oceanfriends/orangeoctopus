﻿using Microsoft.AspNetCore.Http;
using OrangeOctopus.Api.Contracts;
using OrangeOctopus.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace OrangeOctopus.Api.Service
{
    public interface IExcelAuthProvider
    {
        AuthProperties GetAuthProperties(IdentityUser user, DriveType type);
    }

    public class TempExcelAuthProvider : IExcelAuthProvider
    {
        public AuthProperties GetAuthProperties(IdentityUser user, DriveType type)
        {
            return new AuthProperties
            {
                Authority = "https://login.microsoftonline.com/" + "750cfc55-ab3a-4785-986a-aca285210979", // base url + tenant Id
                ApplicationId = "315039c5-45e3-4f9f-a197-a8bf2d80c04a", // calculation-service
                ApplicationSecret = "xcR8num62NbBqjqshjNdVer",
            };

        }

    }
}
