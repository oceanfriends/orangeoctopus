﻿using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OrangeOctopus.Api.Contracts;
using OrangeOctopus.Operations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Api.ModelBinders
{
    public class OperationDtoBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
            {
                throw new ArgumentNullException(nameof(bindingContext));
            }
            bindingContext.HttpContext.Request.EnableRewind();
            string bodyContent = new StreamReader(bindingContext.HttpContext.Request.Body).ReadToEnd();

            try
            {
                var o = JObject.Parse(bodyContent);
                OperationType opType = (OperationType)Enum.Parse(typeof(OperationType), o["operationType"].ToString());
                switch (opType)
                {
                    case OperationType.Excel:
                        var excelOp = o.ToObject<ExcelOperationDto>();
                        bindingContext.Result = ModelBindingResult.Success(excelOp);
                        return Task.CompletedTask;
                    default:
                        var op = o.ToObject<OperationDto>();
                        bindingContext.Result = ModelBindingResult.Success(op);
                        return Task.CompletedTask;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
