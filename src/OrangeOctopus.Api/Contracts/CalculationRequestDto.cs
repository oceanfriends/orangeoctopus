﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Api.Contracts
{
    public class CalculationRequestDto
    {
        public CalculationRequestDto()
        {
            Inputs = new Dictionary<string, object>();
        }
        public string OperationId { get; set; }
        public Dictionary<string, object> Inputs { get; set; }
        
    }
}
