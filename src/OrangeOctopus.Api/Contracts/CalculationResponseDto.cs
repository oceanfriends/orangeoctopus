﻿using Benj.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Api.Contracts
{
    public class CalculationResponseDto : ITraceable
    {
        public CalculationResponseDto()
        {
            Outputs = new Dictionary<string, object>();
        }
        public Guid? CorrelationId { get; set; }
        public string OperationId { get; set; }
        public Dictionary<string, object> Outputs { get; set; }
    }
}
