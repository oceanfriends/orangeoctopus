using System;
using System.Collections.Generic;
using OrangeOctopus.Operations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Microsoft.AspNetCore.Mvc;
using OrangeOctopus.Api.ModelBinders;

namespace OrangeOctopus.Api.Contracts 
{
    [ModelBinder(BinderType = typeof(OperationDtoBinder))]
    public class OperationDto
    {
        /// <summary>
        /// Unique identifier for the dto
        /// </summary>
        public string Id {get; set; }

        /// <summary>
        /// Unique name of the operation definition
        /// </summary>
        public string Name {get; set;}

        /// <summary>
        /// Allowed values:
        ///     Expression
        ///     HttpGet
        ///     HttpPost
        ///     Excel
        /// Matches to upper and lower case, although be a doll and provide the proper case
        /// </summary>
        public OperationType OperationType {get; set;}

        /// <summary>
        /// 
        /// </summary>
        public string Description {get; set; }

        /// <summary>
        /// Related tags
        /// </summary>
        public IEnumerable<string> Tags { get; set; }

        /// <summary>
        /// Basically the endpoint, but named location for 
        /// </summary>
        public string EndpointTemplate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<InputOutputDto> InputDefinitions { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<InputOutputDto> OutputDefinitions { get; set; }
    }
}
