﻿using OrangeOctopus.Operations.Contracts;
using OrangeOctopus.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Api.Contracts
{
    public class ExcelOperationDto : OperationDto
    {
        public string WorkbookId { get; set; }
        public DriveType DriveType { get; set; }
        public Dictionary<string, ExcelCell> CellMappings { get; set; }
    }

    public enum DriveType
    {
        Global,
        Company,
        User
    }
}
