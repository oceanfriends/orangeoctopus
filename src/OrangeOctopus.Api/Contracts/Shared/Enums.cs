namespace OrangeOctopus.Api 
{
    public enum ArgumentStructure { Empty, Single, List, Table };
    public enum ArgumentType {Empty, Number, String, Bool, Object, DateTime };

}
