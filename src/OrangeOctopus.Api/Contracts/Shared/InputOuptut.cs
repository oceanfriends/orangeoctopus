using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace OrangeOctopus.Api 
{
    public class InputOutputDto
    {
        public string Name { get; set; }
        public bool Required { get; set; }
        public string Description {get; set; }
        public ArgumentStructure Structure { get; set; }
        public ArgumentType Type { get; set; }
    }

}


