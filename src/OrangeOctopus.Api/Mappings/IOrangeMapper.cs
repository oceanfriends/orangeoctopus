﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Api.Mappings
{
    public interface IOrangeMapper
    {
        TDestination Map<TSource, TDestination>(TSource value);       
    }
}
