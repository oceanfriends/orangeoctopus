using System;
using AutoMapper;
using OrangeOctopus.Operations;
using OrangeOctopus.Operations.Contracts;
using OrangeOctopus.Api;
using OrangeOctopus.Api.Contracts;
using OrangeOctopus.Shared;

namespace OrangeOctopus.Api.Mapping
{
    /// <summary>
    /// Maps the related entities necessary for creating and interacting with contracts
    /// </summary>
    public class OperationContractProfile : Profile
    {
        public OperationContractProfile()
        {
            CreateMap<TransputDefinition, InputOutputDto>()
                .ForMember(dest => dest.Structure, opt => opt.ResolveUsing<ArgumentStructureResolver, StructureType>(src => src.StructureType))
                .ForMember(dest => dest.Type, opt => opt.ResolveUsing<ArgumentTypeResolver, TypeCode>(src => src.TypeCode));

            CreateMap<InputOutputDto, TransputDefinition>()
                .ForMember(dest => dest.StructureType, opt => opt.ResolveUsing<StructureTypeResolver, ArgumentStructure>(src => src.Structure))
                .ForMember(dest => dest.TypeName, opt => opt.Ignore())
                .ForMember(dest => dest.TypeCode, opt => opt.ResolveUsing<TypeCodeResolver, ArgumentType>(src => src.Type));

            CreateMap<OperationContract, OperationDto>()
                .ForMember(dest => dest.Description, opt => opt.MapFrom(o => o.Meta.Description))
                .ForMember(dest => dest.EndpointTemplate, opt => opt.MapFrom(o => o.Meta.Endpoint))
                .ForMember(det => det.Tags, opt => opt.Ignore());

            CreateMap<OperationDto, OperationContract>()
                .ForMember(dest => dest.Meta, opt => opt.ResolveUsing(o =>
                {
                    return new OperationMeta()
                    {
                        Description = o.Description,
                        OperationId = o.Id,
                        Endpoint = o.EndpointTemplate

                    };
                }));

        }
    }


    public class ArgumentStructureResolver : IMemberValueResolver<object, object, StructureType, ArgumentStructure>
    {
        public ArgumentStructure Resolve(object source, object dest, StructureType sourceMember, ArgumentStructure destMember, ResolutionContext context)
        {
            if (sourceMember.Equals(StructureType.Empty)) return ArgumentStructure.Empty;
            if (sourceMember.Equals(StructureType.Single)) return ArgumentStructure.Single;
            if (sourceMember.Equals(StructureType.Range)) return ArgumentStructure.List;
            if (sourceMember.Equals(StructureType.Complex)) return ArgumentStructure.Single;
            if (sourceMember.Equals(StructureType.ComplexRange)) return ArgumentStructure.Table;
            throw new ArgumentOutOfRangeException("You passed in a structure type which does not exist!");
        }
    }

    public class StructureTypeResolver : IMemberValueResolver<object, object, ArgumentStructure, StructureType>
    {
        public StructureType Resolve(object source, object dest, ArgumentStructure sourceMember, StructureType destMember, ResolutionContext context)
        {
            if (sourceMember.Equals(ArgumentStructure.Empty)) return StructureType.Empty;
            if (sourceMember.Equals(ArgumentStructure.Single)) return StructureType.Single;
            if (sourceMember.Equals(ArgumentStructure.List)) return StructureType.Range;
            if (sourceMember.Equals(ArgumentStructure.Table)) return StructureType.ComplexRange;
            throw new ArgumentOutOfRangeException("You passed in a structure type which does not exist!");
        }
    }

    public class TypeCodeResolver : IMemberValueResolver<object, object, ArgumentType, TypeCode>
    {
        public TypeCode Resolve(object source, object dest, ArgumentType sourceMember, TypeCode destMember, ResolutionContext context)
        {
            switch (sourceMember)
            {
                case ArgumentType.Empty:
                    return TypeCode.Empty;
                case ArgumentType.Number:
                    return TypeCode.Double;
                case ArgumentType.String:
                    return TypeCode.String;
                case ArgumentType.Bool:
                    return TypeCode.Boolean;
                case ArgumentType.DateTime:
                    return TypeCode.DateTime;
                case ArgumentType.Object:
                    return TypeCode.Object;
                default:
                    throw new ArgumentOutOfRangeException(nameof(sourceMember), "You passed in an argument type which does not exist!");
            }
        }
    }

    public class ArgumentTypeResolver : IMemberValueResolver<object, object, TypeCode, ArgumentType>
    {
        public ArgumentType Resolve(object source, object dest, TypeCode sourceMember, ArgumentType destMember, ResolutionContext context)
        {
            switch (sourceMember)
            {
                case TypeCode.Boolean:
                    return ArgumentType.Bool;
                case TypeCode.DateTime:
                    return ArgumentType.DateTime;
                case TypeCode.Empty:
                    return ArgumentType.Empty;
                case TypeCode.String:
                    return ArgumentType.String;
                case TypeCode.Object:
                    return ArgumentType.Object;

                case TypeCode.Decimal:
                    return ArgumentType.Number;
                case TypeCode.Single:
                    return ArgumentType.Number;
                case TypeCode.Double:
                    return ArgumentType.Number;
                case TypeCode.Int16:
                    return ArgumentType.Number;
                case TypeCode.Int32:
                    return ArgumentType.Number;
                case TypeCode.Int64:
                    return ArgumentType.Number;
                case TypeCode.UInt16:
                    return ArgumentType.Number;
                case TypeCode.UInt32:
                    return ArgumentType.Number;
                case TypeCode.UInt64:
                    return ArgumentType.Number;

                default:
                    throw new ArgumentOutOfRangeException("You passed in a structure type which does not exist!");
            }
        }
    }
}
