﻿using OrangeOctopus.Api.Contracts;
using OrangeOctopus.Operations;
using OrangeOctopus.Operations.Contracts;
using OrangeOctopus.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Api.Mappings
{
    public class Enrichers
    {
        public static OperationContract EnrichOperationFromDto(OperationDto dto, OperationContract contract)
        {
            if (dto is ExcelOperationDto edto)
            {
                var excelMeta = new ExcelOperationMeta(edto.CellMappings, tempExcelShit, tempExcelDriveIdBS, edto.WorkbookId)
                {
                    Description = edto.Description,
                };
                var excelOp = new ExcelOperationContract(contract, excelMeta);
                return excelOp;
            }

            return contract;

        }
        private static AuthProperties tempExcelShit = new AuthProperties
            {
                Authority = "https://login.microsoftonline.com/" + "750cfc55-ab3a-4785-986a-aca285210979", // base url + tenant Id
                ApplicationId = "315039c5-45e3-4f9f-a197-a8bf2d80c04a", // calculation-service
                ApplicationSecret = "xcR8num62NbBqjqshjNdVer",
            };
        private static string tempExcelDriveIdBS = "b!UDtp5_XzbUGVF6Y4Pun6OK3B4nati_lAlio4QYgNvAexUC1OoHD1T5xQ3Y9YnwJL";
    }
}
