﻿using AutoMapper;
using Benj.Common;
using System;
using System.Collections.Generic;

namespace OrangeOctopus.Api.Mappings
{
    public class OrangeMapper : IOrangeMapper
    {
        private readonly IMapper _automapper;
        public OrangeMapper(IMapper automapper)
        {
            _automapper = automapper;
        }

        public TypeDictionary<TypeDictionary<object>> _enrichers = new TypeDictionary<TypeDictionary<object>>();

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TDestination"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public TDestination Map<TSource, TDestination>(TSource value)
        {
            var automapped = _automapper.Map<TDestination>(value);
            if (!_enrichers.TryGet<TSource>(out TypeDictionary<object> sourceMaps)) return automapped;
            if (!sourceMaps.TryGet<TDestination>(out object sourceToDestMap)) return automapped;
            return ((Func<TSource, TDestination, TDestination>)sourceToDestMap)(value, automapped);
        }

        /// <summary>
        /// Adds a custom func to execute after the base type mapping has occured. Useful for modifying types
        /// and replacing instance members
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TDestination"></typeparam>
        /// <param name="enricher"></param>
        public void AddEnricher<TSource, TDestination>(Func<TSource, TDestination ,TDestination> enricher)
        {
            if (!_enrichers.ContainsKey(typeof(TSource))) _enrichers.Set<TSource>(new TypeDictionary<object>());
            var sourceMap = _enrichers.Get<TSource>();
            sourceMap.Set<TDestination>(enricher);
        }
    }
}
