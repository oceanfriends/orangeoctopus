﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Calculation.Helpers
{
    public static class ExcelHelpers
    {
        public static async Task<string> CreateSession(string baseExcelUrl, string driveId, string workbookId, bool persistChanges, string accessToken)
        {
            var url = $"{baseExcelUrl}/drives/{driveId}/items/{workbookId}/workbook/createSession";
            var body = new { persistChanges = persistChanges };

            var result = await DoExcelRequestAsync(HttpMethod.Post, body, url, accessToken, null);

            dynamic json = JObject.Parse(result);
            return json.id;
        }

        public static async Task<string> GetNamedItemAddress(string name, string baseExcelUrl, string driveId, string workbookId, string accessToken, string sessionId)
        {
            // Setup and execute http request
            var restUrl = $"{baseExcelUrl}/drives/{driveId}/items/{workbookId}/workbook/names('{name}')/range";
            var result = await DoExcelRequestAsync(HttpMethod.Get, null, restUrl, accessToken, sessionId);

            // Get the address of the named item from the response
            dynamic json = JObject.Parse(result);
            return json.address;
        }

        public static async Task<object> GetNamedItemValue(string name, string baseExcelUrl, string driveId, string workbookId, string accessToken, string sessionId)
        {
            // Setup and execute http request
            var restUrl = $"{baseExcelUrl}/drives/{driveId}/items/{workbookId}/workbook/names('{name}')/range";
            var result = await DoExcelRequestAsync(HttpMethod.Get, null, restUrl, accessToken, sessionId);

            // Get the value for the named item from the response
            dynamic json = JObject.Parse(result);
            return json["values"][0][0];
        }

        public static async Task<object> GetRangeValue(string address, string baseExcelUrl, string driveId, string workbookId, string accessToken, string sessionId)
        {
            // Address format: worksheet!cell
            var worksheet = address.Split('!')[0];
            var cellAddress = address.Split('!')[1];

            // Setup and execute http request
            var restUrl = $"{baseExcelUrl}/drives/{driveId}/items/{workbookId}/workbook/worksheets('{worksheet}')/range(address='{cellAddress}')";
            var result = await DoExcelRequestAsync(HttpMethod.Get, null, restUrl, accessToken, sessionId);

            // get the value for the address from te
            JObject json = JObject.Parse(result);
            return json["values"][0][0];
        }

        public static async Task SetCellValue(string address, string value, string baseExcelUrl, string driveId, string workbookId, string accessToken, string sessionId)
        {
            // Address format: worksheet!cell
            var worksheet = address.Split('!')[0];
            var cellAddress = address.Split('!')[1];

            // TODO: Validate that the cell address is for a single cell

            // update the worksheet cell with the new value using Patch
            var restUrl = $"{baseExcelUrl}/drives/{driveId}/items/{workbookId}/workbook/worksheets('{worksheet}')/range(address='{cellAddress}')";

            var values = new string[,] { { value } };
            var body = new { values = values };

            var result = await DoExcelRequestAsync(new HttpMethod("PATCH"), body, restUrl, accessToken, sessionId);
        }

        private static async Task<string> DoExcelRequestAsync(HttpMethod method, object body, string url, string accessToken, string sessionId)
        {
            string result = string.Empty;

            using (var client = new HttpClient())
            {
                using (var request = new HttpRequestMessage(method, url))
                {
                    // Add authorization header
                    request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

                    // Add excel session header if provided
                    if (!string.IsNullOrEmpty(sessionId))
                    {
                        request.Headers.Add("workbook-session-id", sessionId);
                    }

                    // Add body request
                    if (body != null)
                    {
                        request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
                    }

                    // Make request and pull result from response body
                    using (HttpResponseMessage response = await client.SendAsync(request))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = await response.Content.ReadAsStringAsync();
                        }
                    }
                }
            }

            return result;
        }
    }
}
