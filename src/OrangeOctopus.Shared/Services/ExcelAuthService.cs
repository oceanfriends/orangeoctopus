﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Shared
{
    public class ExcelAuthService : IAuthService
    {
        public ExcelAuthService()
        {

        }

        public async Task<string> GetUserAccessToken(AuthProperties authProperties)
        {
            AuthenticationContext authContext = new AuthenticationContext(authProperties.Authority);
            ClientCredential credential = new ClientCredential(authProperties.ApplicationId, authProperties.ApplicationSecret);

            try
            {
                AuthenticationResult authResult = await authContext.AcquireTokenAsync(
                    "https://graph.microsoft.com",
                    credential
                );
                return authResult.AccessToken;
            }
            catch(Exception ex)
            {
                throw new Exception("Error authenticating.", ex);
            }
        }
    }
}
