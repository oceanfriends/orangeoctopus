﻿namespace OrangeOctopus.Shared
{
    public class AuthProperties
    {
        public static AuthProperties _andrewAuthProperties = 
        new AuthProperties
        {
            Authority = "https://login.microsoftonline.com/" + "750cfc55-ab3a-4785-986a-aca285210979", // base url + tenant Id
            ApplicationId = "315039c5-45e3-4f9f-a197-a8bf2d80c04a", // calculation-service
            ApplicationSecret = "xcR8num62NbBqjqshjNdVer",
        };
        // Logon authority
        public string Authority { get; set; }

        // Application ID
        public string ApplicationId { get; set; }

        // Application Secret
        public string ApplicationSecret { get; set; }

        // User Object Id
        public string UserObjectId { get; set; }
    }
}
