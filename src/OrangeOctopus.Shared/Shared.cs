﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Benj.Common;
using Benj.Common.Entities;

namespace OrangeOctopus.Shared
{
    public class Helper
    {

        public static ActivityResult<T> Failure<T>(string message) 
        {
            var rtn = new ActivityResult<T>(default(T), StatusCode.Failed);
            rtn.Message = message;
            return rtn;
        }
        public static ActivityResult<T> Error<T>(Exception ex, string message) 
        {
            var rtn = new ActivityResult<T>(ex);
            rtn.Message = message;
            return rtn;
        }

        public static ActivityResult<T> FailedFromSeed<T>(ActivityResult seed, string message) 
        {
            var rtn = new ActivityResult<T>(seed);
            rtn.Message = message;
            return rtn;
        }

        public static ActivityResult<T> FromAction<T>(Func<T> toExecute, string messageOnFail) 
        {
            try {
                var payload = toExecute();
                return new ActivityResult<T>(payload);
            }
            catch(Exception ex)
            {
                var rtn = new ActivityResult<T>(ex);
                rtn.Message = messageOnFail;
                return rtn;
            }
        }
        
    }
}
