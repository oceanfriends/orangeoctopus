﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Data.Search
{
    public abstract class BaseSearchContract
    {
        public string OrganizationId { get; set; }
        public string UserId { get; set; }
    }
}
