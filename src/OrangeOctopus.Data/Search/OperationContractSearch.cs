﻿using Benj.Common.Data;
using OrangeOctopus.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeOctopus.Data.Search
{
    public class OperationContractSearch : BaseSearchContract, ISearchSpec<OperationContract>
    {
    }
}
