﻿using Aq.ExpressionJsonSerializer;
using Benj.Common.Data;
using Newtonsoft.Json;
using OrangeOctopus.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Benj.Common;

namespace OrangeOctopus.Data.Work
{
    public class TempRepo : IAmARepo
    {
        private SimpleSerializingRepo repo;
        public TempRepo()
        {
            var settings = new JsonSerializerSettings();
            settings.Converters.Add(new ExpressionJsonConverter(Assembly.GetAssembly(typeof(Transput))));
            repo = new SimpleSerializingRepo(x => x.GetHashCode().ToString(), settings);
        }

        public void AddKeyStrategy<T>(Func<T, string> keySelector) => repo.AddKeyStrategy<T>(keySelector);

        public ActivityResult<IEnumerable<T>> All<T>(SearchOption options = null)
        {
            throw new NotImplementedException();
        }

        public IAtomicOperation BeginAtomicOperation()
        {
            return ((ICanCrud)repo).BeginAtomicOperation();
        }

        public ActivityResult<T> Create<T>(T toSave)
        {
            return ((ICanCrud)repo).Create(toSave);
        }

        public ActivityResult<T> Create<T>(T toSave, string key)
        {
            throw new NotImplementedException();
        }

        public ActivityResult Delete<T>(string key)
        {
            return ((ICanCrud)repo).Delete<T>(key);
        }

        public ActivityResult<T> Get<T>(string key)
        {
            return ((ICanCrud)repo).Get<T>(key);
        }

        public ActivityResult<IEnumerable<T>> Search<T>(ISearchSpec<T> spec, SearchOption options = null)
        {
            throw new NotImplementedException();
        }

        public ActivityResult<T> Update<T>(T toSave)
        {
            return ((ICanCrud)repo).Update(toSave);
        }

        public ActivityResult<T> Update<T>(T toSave, string key)
        {
            throw new NotImplementedException();
        }
    }
}
